from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from api.views import BookDocumentView

admin.site.site_header = "Agnito"
admin.site.site_title = "Agnito"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('frontend.urls')),
    path('superadmin/', include('backend.backend.urls')),
    path('admin-login/', include('login.urls')),
    path('api/', include('api.urls')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('search/', BookDocumentView.as_view({'get': 'list'})),
    path('accounts/', include('allauth.urls')), ### allauth
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
