from backend.user_manager.models import User
from backend.book_category_manager.models import Book_category
from tzlocal import get_localzone
from frontend.models import Wishlist


def get_user(request):
    tz = get_localzone()
    tz = "Asia/Calcutta"
    #print(tz)
    wishlist_count = Wishlist.objects.filter(user_id=request.user.id).count()
    parent_category = Book_category.objects.filter(parent_category_id=None, show_on_ui=True).values('id', 'name').order_by('-name')
    for parent in parent_category:
        parent['child'] = Book_category.objects.filter(parent_category_id=parent['id'], show_on_ui=True).values('id', 'name').order_by('-name')
        for child in parent['child']:
            if child['id'] != 9:
                child['child'] = Book_category.objects.filter(parent_category_id=child['id'], show_on_ui=True).values('id', 'name').order_by('name')
            else:
                child['child'] = Book_category.objects.filter(parent_category_id=child['id'], show_on_ui=True).extra(select={'nameint': 'CAST(name AS UNSIGNED)'}).order_by('nameint').values('id', 'name')

    subject_list = Book_category.objects.filter(cat=1).values('id', 'name')
    board_list = Book_category.objects.filter(cat=2).values('id', 'name')
    classes_list = Book_category.objects.filter(cat=3).values('id', 'name')
    bachelors_list = Book_category.objects.filter(cat=4).values('id', 'name')
    TOKEN = request.session.get('TOKEN', None)
    return {'TOKEN': TOKEN, 'subject_list': subject_list, 'board_list': board_list,
            'classes_list': classes_list, 'bachelors_list': bachelors_list,
            'local_tz': str(tz), 'parent_category':parent_category, 'wishlist': wishlist_count}
