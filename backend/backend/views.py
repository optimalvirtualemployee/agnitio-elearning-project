from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator


def logout_view(request):
    # del request.session['TOKEN']
    logout(request)
    return redirect('/admin-login/')


class SuperAdminView(View):

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request):
        return render(request, 'dashboard.html')

    '''
    # TODO : Showing 404 error in case of Page broken
    def error_404(request, exception):
            data = {}
            return render(request, 'error_404.html')


    # TODO : Showing 500 error in case of Page broken
    def error_500(request, exception):
        data = {}
        return render(request, 'error_500.html')
    
    '''
