from django.urls import path, include
from .views import *

urlpatterns = [
    path('', SuperAdminView.as_view(), name="superadmin"),
    path('logout/', logout_view, name="logout"),
    path('book/', include('backend.book_manager.urls')),
    path('category/', include('backend.book_category_manager.urls')),
    path('user/', include('backend.user_manager.urls')),
    path('payment/', include('backend.payment.urls')),
    path('report/', include('backend.report_management.urls')),
    path('contact/', include('backend.contact.urls')),
    path('banner/', include('backend.banner.urls')),
    path('teacher/', include('backend.teacher_access.urls')),
    path('book_assign/', include('backend.book_assign.urls')),
]