from django.db import models
from backend.book_category_manager.models import Book_category
from backend.user_manager.models import User
import epub_meta
from .manage import FileManager
from .getpy import GetEngine
from .pdfpy import PdfEngine
import os
import uuid
import django
from django.core.exceptions import ValidationError
import ebooklib
from ebooklib import epub
from django.core.files.images import get_image_dimensions
from random import randint


def validate_cover_file_size(value):
    # ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    # valid_extensions = ['.epub', '.pdf']
    # if not ext.lower() in valid_extensions:
    #     raise ValidationError('Unsupported file extension.')
    w, h = get_image_dimensions(value)
    if w != 100:
        raise ValidationError("The image is %i pixel wide. It's supposed to be 100px" % w)
    if h != 200:
        raise ValidationError("The image is %i pixel high. It's supposed to be 200px" % h)
    return value

def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.epub', '.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


class Readerpubfile:

    # init method or constructor
    def __init__(self, epubfile):
        self.epubfile = epubfile

    def read_epub_file(self):
        try:
            data = {}
            # epub_meta_data = epub_meta.get_epub_metadata(self.epubfile, read_cover_image=True, read_toc=True)
            book = epub.read_epub(self.epubfile)

            creator = book.get_metadata('DC', 'creator')
            identifier = book.get_metadata('DC', 'identifier')
            language = book.get_metadata('DC', 'language')
            pubdate = book.get_metadata('DC', 'pubdate')
            publisher = book.get_metadata('DC', 'publisher')
            subject = book.get_metadata('DC', 'subject')
            title = book.get_metadata('DC', 'title')
            version = book.get_metadata('DC', 'version')

            data['author_name'] = creator[0][0] if creator else ''
            # data['author_name'] = str(epub_meta_data.authors)
            data['identifiers'] = identifier[0][0] if identifier else ''
            data['language'] = language[0][0] if language else ''
            data['publication_date'] = pubdate[0][0] if pubdate else ''
            data['publisher'] = publisher[0][0] if publisher else ''
            data['subject'] = subject[0][0] if subject else ''
            data['version'] = version[0][0] if version else ''
            # data['version'] = str(epub_meta_data.epub_version)
            data['title'] = title[0][0] if title else ''
            return data
        except epub_meta.EPubException as e:
            print(e)
        return


class Book(models.Model):
    cover_file = models.ImageField(validators=[validate_cover_file_size])
    epub_file = models.FileField(null=True, blank=True,
                                 validators=[validate_file_extension])
    epub_sample_file = models.FileField(null=True, blank=True,
                                        validators=[validate_file_extension])
    popular = models.IntegerField(default=0)
    meta_data = models.TextField(null=True, blank=True)
    category = models.ManyToManyField(Book_category, related_name='category_%(class)ss',
                                      blank=True)
    version = models.CharField(max_length=150, null=True, blank=True)
    author_name = models.CharField(max_length=150, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    identifier_name = models.CharField(max_length=250, null=True, blank=True)
    language = models.CharField(max_length=100, null=True, blank=True)
    publication_date = models.CharField(max_length=100, null=True, blank=True)
    reference_id = models.CharField(max_length=100, null=True, blank=True)
    contributors = models.CharField(max_length=100, null=True, blank=True)
    pre_order = models.CharField(max_length=100, null=True, blank=True)
    publisher = models.CharField(max_length=100, null=True, blank=True)
    subject = models.CharField(max_length=100, null=True, blank=True)
    territories = models.CharField(max_length=50, null=True, blank=True)
    territory_out = models.BooleanField(default=False)
    price_by_currency = models.FloatField(null=True, blank=True, default=0)
    price_in_inr = models.FloatField(null=True, blank=True, default=0)
    change_history = models.TextField(null=True, blank=True)
    availability = models.BooleanField(default=False)
    asin = models.CharField(max_length=100, null=True, blank=True)
    tax = models.IntegerField(null=True, blank=True, default=0)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name='created_%(class)ss')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name='updated_%(class)ss')

    # New ffields added from Excel file
    eISBN =  models.CharField(max_length=150, null=True, blank=True)
    related_print_ISBN =  models.CharField(max_length=150, null=True, blank=True)
    sub_title =  models.CharField(max_length=150, null=True, blank=True)
    volume =  models.CharField(max_length=150, null=True, blank=True)
    edition =  models.CharField(max_length=150, null=True, blank=True)
    edited_by =  models.CharField(max_length=150, null=True, blank=True)
    imprint =  models.CharField(max_length=150, null=True, blank=True)
    description =  models.TextField(null=True, blank=True)
    keywords =  models.TextField(null=True, blank=True)
    no_of_reviews = models.FloatField(null=True, blank=True, default=None) #models.IntegerField(default=0)
    review_description = models.TextField(null=True, blank=True)
    review_user  = models.TextField(null=True, blank=True)
    review_date = models.CharField(max_length=150, null=True, blank=True)
    pre_order_release_date = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f"Title: {self.title} {self.id}"

    def save(self, *args, **kwargs):
        if not self.pk and self.epub_file:
            if str(self.epub_file).endswith('.epub'):
                reader = Readerpubfile('media/'+str(self.epub_file))
                #epub_to_pdf('media/'+str(self.epub_file))
                data = reader.read_epub_file()

                if data is not None:
                    self.author_name = data.get('author_name')
                    # self.author_name = eval(data.get('author_name'))[0] if data.get('author_name') else ''
                    self.language = data.get('language')
                    self.publication_date = data.get('publication_date')
                    self.publisher = data.get('publisher')
                    self.subject = data.get('subject')
                    # self.subject = eval(data.get('subject'))[0] if data.get('subject') else ''
                    self.version = data.get('version')
                    self.title = data.get('title')
                    # self.identifier_name = eval(data.get('identifiers'))[0] if data.get('identifiers') else ''
                    self.identifier_name = data.get('identifiers')
                    self.asin = random_string()
        super(Book, self).save(*args, **kwargs)


def random_string(n=10):
    """Returns a random string of length string_length."""
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

    # random = str(uuid.uuid4())
    # random = random.upper()
    # random = random.lower()
    # random = random.replace("-", "")
    # return random[0:string_length]



#TODO : To conevrt a pdf file from epub 
def epub_to_pdf(epub_file):
    file = FileManager(epub_file)
    file.epub_to_zip()
    file.get_directory()
    file.extract_zip()
    engine = GetEngine(file.directory)
    engine.get_all()
    engine.get_html()
    engine.get_pdf()
    engine.get_css()
    engine.get_images()
    pdf = PdfEngine(engine.html_files, engine.css_files,
                    engine.pdf_files, file.directory)
    pdf.convert()
    pdf.combine()
    pdf.del_pdf()
    file.zip_to_epub()
    file.del_directory()

    print('--- Epub to PDF conversion successful')


class CatalogueManager(models.Model):
    cover_file = models.ImageField()
    epub_file = models.FileField(null=True, blank=True,
                                 validators=[validate_file_extension])
    meta_data = models.TextField(null=True, blank=True)
    category = models.ForeignKey(Book_category, on_delete=models.CASCADE, related_name='category_%(class)ss',
                                 null=True, blank=True)
    version = models.CharField(max_length=150, null=True, blank=True)
    author_name = models.CharField(max_length=150, null=True, blank=True)
    identifier_name = models.CharField(max_length=250, null=True, blank=True)
    language = models.CharField(max_length=100, null=True, blank=True)
    publication_date = models.CharField(max_length=100, null=True, blank=True)
    publisher = models.CharField(max_length=100, null=True, blank=True)
    subject = models.CharField(max_length=100, null=True, blank=True)
    is_deleted = models.BooleanField(default=False, null=True, blank=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name='created_%(class)ss')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name='updated_%(class)ss')

    def __str__(self):
        return f'Epub{self.id}'


class Territory(models.Model):
    types = [
        ("IND","All India"),
        ("OIND","Rest Of the World")
    ]
    name = models.CharField(max_length=150)
    territory_type = models.CharField(max_length=4,choices=types,default="IND")

    def __str__(self):
        return self.name



class NoteContent(models.Model):
    note_id = models.TextField(null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE, null=True, blank=True,related_name="book")
    accessTime = models.DateTimeField(default=django.utils.timezone.now)
    cfiRange = models.TextField(null=True, blank=True)
    chpaterName = models.TextField(null=True, blank=True)
    color = models.CharField(max_length=100, blank=True, null=True)
    createTime = models.DateTimeField(default=django.utils.timezone.now)
    key = models.TextField(null=True, blank=True)
    pageNum = models.CharField(max_length=255)
    paragraphCfi = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name_plural = "Note Content"


class NoteData(models.Model):
    key = models.ForeignKey(NoteContent, on_delete=models.CASCADE,related_name="note_content")
    note_content = models.TextField(null=True, blank=True)
    createTime = models.DateTimeField(default=django.utils.timezone.now)

    def __str__(self):
        return self.note_content
    class Meta:
        verbose_name_plural = "Note Data"

