# from io import open_code
import os
from django.views import View
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from .models import Book, Territory
from backend.book_category_manager.models import Book_category
from django.contrib.auth.decorators import login_required
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.conf import settings
import xlrd
from .models import random_string, Readerpubfile
from datetime import datetime


@login_required
@admin_logged
def get_catalogue(request):
    template_name = 'catalogue_manager.html'
    book_list = Book.objects.filter(is_deleted=False).order_by('-id')
    return render(request, template_name, {'book_list': book_list, 'catalogue': True})


class CatalogueView(View):
    template_name = 'add_catalogue_manager.html'
    context = {}

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        # cat_list = Book_category.objects.all()
        cat_list = Book_category.objects.filter(parent_category_id=None)
        cat_list2 = Book_category.objects.filter(parent_category_id__in=cat_list)
        cat_list3 = Book_category.objects.filter(parent_category_id__in=cat_list2)
        self.context['cat_list'] = cat_list
        if pk is not None:
            self.template_name = 'edit_catalogue_manager.html'
            try:
                book_obj = Book.objects.get(id=pk)
                territory_ind = Territory.objects.values('id', 'name').filter(territory_type='IND')
                territory_oind = Territory.objects.values('id', 'name').filter(territory_type='OIND')
            except Book.DoesNotExist:
                book_obj = None
            self.context['book_obj'] = book_obj
            self.context['cat_list2'] = cat_list2
            self.context['cat_list3'] = cat_list3
            self.context['territory_ind'] = territory_ind
            self.context['territory_oind'] = territory_oind
            self.context['catalogue'] = True
            self.context['book_manager'] = False
        else:
            self.context['book_manager'] = True
            self.context['catalogue'] = False

        return render(request, self.template_name, self.context)

    @method_decorator(login_required)
    def post(self, request):
        book_id = request.POST.get('book_id')
        try:
            language = ""
            title = ""
            sub_title = ""
            version = ""
            subject = ""
            identifier_name = ""
            publisher = ""
            publication_date = ""
            author_name = ""
            reference_id = ""
            contributors = ""
            pre_order = ""
            eISBN = ""
            related_print_ISBN = ""
            volume = ""
            edition = ""
            edited_by = ""
            imprint = ""
            description = ""
            keywords = ""
            pre_order_release_date = ""
            no_of_reviews = ""
            review_description = ""
            review_user = ""
            review_date = ""
            data = {}
            epub_file = request.FILES.get('epub_file')  # dsp
            epub_sample_file = request.FILES.get('epub_sample_file')  # dsp

            cover_file = request.FILES.get('cover_file')
            try:
                book_obj = Book.objects.get(id=book_id)
                book_obj.updated_by_id = request.user.id if request.user else 1
            except Book.DoesNotExist:
                book_obj = Book()
                book_obj.created_by_id = request.user.id if request.user else 1

            fs = FileSystemStorage()
            if epub_file:
                if not 'epub' in str(epub_file.content_type):
                    messages.error(request, 'File type is not epub.')
                    return redirect('ebook:book_manager')
                fs.save(epub_file.name, epub_file)
                book_obj.epub_file = epub_file.name
                # book_obj.epub_file = epub_file.name.split('.')[0] + '.pdf'
            if cover_file:
                fs.save(cover_file.name, cover_file)
                book_obj.cover_file = cover_file.name
            if epub_sample_file:
                if not 'epub' in str(epub_sample_file.content_type):
                    messages.error(request, 'File type is not epub.')
                    return redirect('ebook:book_manager')
                fs.save(epub_sample_file.name, epub_sample_file)
                book_obj.epub_sample_file = epub_sample_file.name

            if str(epub_file).endswith('.epub'):
                reader = Readerpubfile('media/' + str(epub_file))
                # epub_to_pdf('media/'+str(self.epub_file))
                data = reader.read_epub_file()

            asin = request.POST.get('asin', random_string())
            change_history = request.POST.get('change_history')
            if request.POST.get('language'):
                language = request.POST.get('language', data.get('language'))
            form_type = request.POST.get('form_type')
            price_by_currency = request.POST.get('price_by_currency')
            price_in_inr = request.POST.get('price_in_inr')
            territories = request.POST.get('territories')
            meta_data = request.POST.get('meta_data')
            category_id = request.POST.getlist('category[]')
            if request.POST.get('title'):
                title = request.POST.get('title', data.get('title'))
            if request.POST.get('version'):
                version = request.POST.get('version', data.get('version'))
            if request.POST.get('author_name'):
                author_name = request.POST.get('author_name', data.get('author_name'))
            if request.POST.get('publisher'):
                publisher = request.POST.get('publisher', data.get('publisher'))
            if request.POST.get('identifier_name'):
                identifier_name = request.POST.get('identifier_name', data.get('identifiers'))
            if request.POST.get('subject'):
                subject = request.POST.get('subject', data.get('subject'))
            if request.POST.get('publication_date'):
                publication_date = request.POST.get('publication_date', data.get('publication_date'))
            reference_id = request.POST.get('reference_id')
            pre_order = request.POST.get('pre_order')
            contributors = request.POST.get('contributors')
            tax = request.POST.get('tax')
            territory_out = request.POST.get('territory_out')
            sub_title = request.POST.get('sub_title', '')
            eISBN = request.POST.get('eISBN')
            related_print_ISBN = request.POST.get('related_print_ISBN')
            volume = request.POST.get('volume')
            edition = request.POST.get('edition')
            edited_by = request.POST.get('edited_by')
            imprint = request.POST.get('imprint')
            description = request.POST.get('description')
            keywords = request.POST.get('keywords')
            no_of_reviews = request.POST.get('no_of_reviews')
            review_description = request.POST.get('review_description')
            review_user = request.POST.get('review_user')
            review_date = request.POST.get('review_date')

            pre_order_release_date = request.POST.get('pre_order_release_date')
            if meta_data:
                book_obj.meta_data = meta_data
            active = request.POST.get('active', '0')
            popular = request.POST.get('popular', '0')
            book_obj.is_active = True if active != '0' else False
            book_obj.popular = 1 if popular != '0' else 0
            if tax:
                book_obj.tax = tax
            if price_by_currency:
                book_obj.price_by_currency = price_by_currency
            if price_in_inr:
                book_obj.price_in_inr = price_in_inr
            if form_type == '2':
                book_obj.territories = territories
                # TODO : Just added 26th sepetember as radio button was not  functioning
                book_obj.territory_out = 1 if territory_out is not None and int(territory_out) == 1 else 0

            if request.POST.get('availability') is not None:
                availability = request.POST['availability']
                book_obj.availability = True if availability == '1' else False
            if change_history:
                book_obj.change_history = change_history
            if asin:
                book_obj.asin = asin
            if language:
                book_obj.language = language
            if title:
                book_obj.title = title
            if sub_title:
                book_obj.sub_title = sub_title
            if version:
                book_obj.version = version
            if subject:
                book_obj.subject = subject
            if identifier_name:
                book_obj.identifier_name = identifier_name
            if publisher:
                book_obj.publisher = publisher
            if publication_date:
                book_obj.publication_date = publication_date
            if author_name:
                book_obj.author_name = author_name
            if reference_id:
                book_obj.reference_id = reference_id
            if contributors:
                book_obj.contributors = contributors
            if pre_order:
                book_obj.pre_order = pre_order
            # TODO : Just added 26th sepetember as radio button was not  functioning
            if eISBN:
                book_obj.eISBN = eISBN
            if related_print_ISBN:
                book_obj.related_print_ISBN = related_print_ISBN
            if volume:
                book_obj.volume = volume
            if edition:
                book_obj.edition = edition
            if edited_by:
                book_obj.edited_by = edited_by
            if imprint:
                book_obj.imprint = imprint
            if description:
                book_obj.description = description
            if keywords:
                book_obj.keywords = keywords
            if pre_order_release_date:
                book_obj.pre_order_release_date = pre_order_release_date
            if no_of_reviews:
                book_obj.no_of_reviews = no_of_reviews
            if review_description:
                book_obj.review_description = review_description
            if review_user:
                book_obj.review_user = review_user
            if review_date:
                book_obj.review_date = review_date

            book_obj.save()
            print(f"Category List => {category_id}")

            if category_id:
                category_id = list(map(int, category_id))
                if book_id:
                    cat_ids = [i.id for i in book_obj.category.all()]
                    book_obj.category.remove(*cat_ids)
                for i in category_id:
                    book_obj.category.add(i)
            if book_id:
                messages.success(request, 'Updated successfully.')
            else:
                messages.success(request, 'Created successfully.')
            return redirect('ebook:get_catalogue_manager')
        except Exception as e:
            print(e)
            messages.error(request, str(e))
            return redirect('ebook:get_catalogue_manager')

    @staticmethod
    def book_delete(request, *args, **kwargs):
        pk = kwargs.get('pk')
        try:
            Book.objects.get(pk=pk).delete()
            messages.success(request, 'Book Deleted successfully.')
            return redirect('ebook:get_catalogue_manager')
        except Exception as e:
            print(e)
            messages.error(request, 'This can not be deleted as it is purchased by user.')
            return redirect('ebook:get_catalogue_manager')


# class EbookView(View):
#
#     def get(self, request):
#         fname = 'media/Around the World in 28 Languages.epub'
#         res = get_epub_info(fname)
#         return HttpResponse(res)
#
#
# def get_epub_info(fname):
#     book = epub.read_epub(fname)
#     items = list(book.get_items_of_type(ebooklib.ITEM_DOCUMENT))
#
#     def chapter_to_str(chapter):
#         soup = BeautifulSoup(chapter.get_body_content(), 'html.parser')
#         text = [para.text.replace('\n', ' ').strip() for para in soup.find_all('p')]
#         return ' '.join(text)
#
#     texts = {}
#     for c in items:
#         texts[c.get_name()] = chapter_to_str(c)
#     print(texts)
#     return texts
#
#
#
# def get_epub_info11(fname):
#     ns = {
#         'n':'urn:oasis:names:tc:opendocument:xmlns:container',
#         'pkg':'http://www.idpf.org/2007/opf',
#         'dc':'http://purl.org/dc/elements/1.1/'
#     }
#
#     # prepare to read from the .epub file
#     zip = zipfile.ZipFile(fname)
#
#     # find the contents metafile
#     txt = zip.read('META-INF/container.xml')
#     tree = etree.fromstring(txt)
#     cfname = tree.xpath('n:rootfiles/n:rootfile/@full-path',namespaces=ns)[0]
#
#     # grab the metadata block from the contents metafile
#     cf = zip.read(cfname)
#     tree = etree.fromstring(cf)
#     p = tree.xpath('/pkg:package/pkg:metadata',namespaces=ns)[0]
#     # repackage the data
#     res = {}
#     for s in ['title','language','creator','date','identifier']:
#         res[s] = p.xpath('dc:%s/text()'%(s),namespaces=ns)[0]
#
#     return res

def import_books(request):
    if request.method == "POST":
        book_file = request.FILES["importData"]
        ext = os.path.splitext(book_file.name)[1]
        print(f"file Ext => {ext}")

        file_size = book_file.size / 1024
        valid_ext = ['.xls', '.xlsx']
        if ext not in valid_ext:
            messages.error(request, 'Unsupported file extension (support only .xls or .xlsx).')
            return redirect('ebook:get_catalogue_manager')
        # elif file_size > 1024:
        #     messages.error(request, 'File size exceeded more than 1MB.')
        #     return render(
        #         request, "admin/tutorial/book/tarining_file_import_form.html", {
        #             'data' : '',
        #             "form": form
        #         }
        #     )
        try:
            with open(settings.MEDIA_ROOT + '/' + book_file.name, 'wb+') as destination:
                for chunk in book_file.chunks():
                    destination.write(chunk)

            workbook = xlrd.open_workbook(settings.MEDIA_ROOT + '/' + book_file.name)
            worksheet = workbook.sheet_by_index(0)

            ## valid Conetnt Types
            # content_type = Book.objects.values_list('title',flat=True).filter(status=1)
            # c_type = [item.lower() for item in content_type]
            final_worksheet = []
            for row in range(worksheet.nrows):
                rows = []
                for col in range(worksheet.row_len(row)):
                    if type(worksheet.cell_value(row, col)) == 'str':
                        rows.append(worksheet.cell_value(row, col).strip())
                    else:
                        rows.append(worksheet.cell_value(row, col))
                final_worksheet.append(rows)

            file_valid_header = {
                'Reference ID': 'reference_id',
                'eISBN (Optional)': 'eISBN',
                'Related Print ISBN': 'related_print_ISBN',
                'Book Title': 'title',
                'Subtitle (Optional)': 'sub_title',
                'Description': 'description',
                'Volume (Optional)': 'volume',
                'Edition': 'edition',
                'Authors': 'author_name',
                'Edited By': 'edited_by',
                'Imrpint': 'imrpint',
                'Publication Date': 'publication_date',
                'Pre-order Release Date': 'pre_order_release_date',
                'Board': 'cat_id',
                'Class': 'cat_id',
                'Subject': 'cat_id',
                'Keywords': 'keywords',
            }
            optional_fields = ['eISBN (Optional)', 'Subtitle (Optional)', 'Volume (Optional)', 'Edited By',
                               'Pre-order Release Date']
            skip_fields = ['Board', 'Class', 'Subject']
            # get file header
            file_header = [item.lower() for item in final_worksheet[0]]
            print(f"File Header => {file_header}")
            header_keys = [item for item in file_valid_header.keys()]
            print(f"header_keys => {header_keys}")
            check = all(item.lower() in file_header for item in header_keys)
            if check is False:
                messages.error(request, 'Invalid File: File header not matched.')
                return redirect('ebook:get_catalogue_manager')
            invalid_records = []
            valid_records = []
            i = 0
            for rows in final_worksheet:
                if i == 0:
                    i = i + 1
                    continue
                j = 0
                book = Book()
                invalid_record = False
                category_ids = []
                for key in final_worksheet[0]:
                    if key not in file_valid_header.keys():
                        j = j + 1
                        continue

                    if key not in optional_fields and key not in skip_fields and rows[j] == '':
                        setattr(book, file_valid_header[key], rows[j].capitalize())
                        invalid_record = True
                    elif key in skip_fields:
                        print(f"{key} => {rows[j]}")
                        category_ids.append(rows[j])
                    else:
                        # print(f"{file_valid_header[key]} => {rows[j]}")
                        # print(f"type => {type(rows[j])}")
                        val = rows[j]
                        val_date = None
                        if isinstance(rows[j], float):
                            val = int(rows[j])
                        if key == 'Publication Date' or key == 'Pre-order Release Date':
                            if rows[j] != '':
                                datetime_date = xlrd.xldate_as_datetime(rows[j], 0)
                                date_object = datetime_date.date()
                                val_date = date_object.isoformat()

                        if val_date != None:
                            setattr(book, file_valid_header[key], val_date)
                        else:
                            if key == "Description":
                                setattr(book, file_valid_header[key], val.encode('unicode_escape'))
                            else:
                                setattr(book, file_valid_header[key], str(val))
                            # setattr(book,file_valid_header[key],val.encode('unicode_escape'))

                    j = j + 1
                if invalid_record:
                    invalid_records.append(rows)
                elif not invalid_record:
                    valid_records.append(rows)
                    print(f"Book Record => {book.__dict__}")
                    book.save()
                    if len(category_ids) > 0:
                        print(f"categories => {category_ids}")
                        for i in category_ids:
                            book.category.add(i)
                else:
                    continue
            messages.success(request, 'File imported successfully.')
            return redirect('ebook:get_catalogue_manager')
        except Exception as e:
            messages.error(request, str(e))
            return redirect('ebook:get_catalogue_manager')
        finally:
            book_file.close()
            os.unlink(settings.MEDIA_ROOT + '/' + book_file.name)
            return redirect('ebook:get_catalogue_manager')
