from django.urls import path, include
from .views import *

app_name = 'ebook'

urlpatterns = [
    path('CatalogueList/', get_catalogue, name="get_catalogue_manager"),
    path('Create/', CatalogueView.as_view(), name="book_manager"),
    path('update/<int:pk>/', CatalogueView.as_view(), name="edit_catalogue_manager"),
    path('delete/<int:pk>/', CatalogueView.book_delete, name="book_delete"),
    path('bulk_import_book/', import_books, name="bulk_import_book"),
]