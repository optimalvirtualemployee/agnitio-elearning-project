from django_elasticsearch_dsl import (
    Document,
    fields,
    Index,
)
from .models import Book

PUBLISHER_INDEX = Index('elastic_demo')

PUBLISHER_INDEX.settings(
    number_of_shards=1,
    number_of_replicas=1
)


@PUBLISHER_INDEX.doc_type
class BookDocument(Document):
    id = fields.IntegerField(attr='id')
    fielddata = True
    author_name = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',
            }

        }
    )
    title = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',
            }
        },
    )
    asin = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',

            }
        },
    )
    subject = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',

            }
        },
    )
    identifier_name = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',

            }
        },
    )
    keywords = fields.TextField(
        fields={
            'raw': {
                'type': 'keyword',

            }
        },
    )

    class Django(object):
        model = Book
