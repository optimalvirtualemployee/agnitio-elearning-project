from .models import Book, CatalogueManager, Territory, NoteContent,NoteData
from django.contrib import admin
from django.utils.html import format_html


class NoteContentAdmin(admin.ModelAdmin):
    list_display = ('note_id','content','book_id','accessTime','chpaterName','color','createTime','pageNum','user')
admin.site.register(NoteContent,NoteContentAdmin)


class NoteDataAdmin(admin.ModelAdmin):
    list_display = ('key','note_content','createTime')
admin.site.register(NoteData,NoteDataAdmin)

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    fields = ['cover_file', 'epub_file', 'epub_sample_file', 'meta_data', 'territory_out', 'popular', 'created_by', 'category']
    list_display = ['action', 'cover_image', 'meta_data', 'created_by_name', 'created_at', 'author_name', 'language',
                    'publication_date', 'publisher', 'popular', 'subject', 'version', 'identifier_name', 'created_at']
    list_display_links = ('action',)
    readonly_fields = ('cover_image',)
    # actions = ('active',)
    # save_on_top = True

    def get_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.fields + ['author_name', 'language',
                    'publication_date', 'publisher', 'subject', 'price_by_currency', 'price_in_inr', 'title', 'version', 'identifier_name', 'asin', 'territories']
        return self.fields

    def active(self):
        pass

    def action(self, obj):
        return format_html(
            f'<a href="/admin/book_manager/book/{obj.id}/change/" class="btn btn-primary">View</a>'
        )

    def cover_image(self, obj):
        return format_html(
            f'<img src="{obj.cover_file.url if obj.cover_file else None}" width="100" height="100">'
        )

    def created_by_name(self, obj):
        return f'{obj.created_by.first_name if obj.created_by else ""} {obj.created_by.last_name if obj.created_by else ""}'


@admin.register(CatalogueManager)
class CatalogueManagerAdmin(admin.ModelAdmin):
    fields = ['cover_file', 'epub_file', 'meta_data', 'created_by']
    list_display = ['action', 'cover_image', 'meta_data', 'created_by_name', 'created_at', 'author_name', 'language',
                    'publication_date', 'publisher', 'subject', 'version', 'identifier_name']
    list_display_links = ('action',)
    readonly_fields = ('cover_image',)
    # actions = ('active',)
    # save_on_top = True

    def get_queryset(self, request):
        qs = Book.objects.all()
        return qs

    def has_add_permission(self, request):
        return False

    def get_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.fields + ['author_name', 'language',
                    'publication_date', 'publisher', 'subject', 'version', 'identifier_name']
        return self.fields

    def active(self):
        pass

    def action(self, obj):
        return format_html(
            f'<a href="/admin/book_manager/book/{obj.id}/change/" class="btn btn-primary">View</a>'
        )

    def cover_image(self, obj):
        return format_html(
            f'<img src="{obj.cover_file.url if obj.cover_file else None}" width="100" height="100">'
        )

    def created_by_name(self, obj):
        return f'{obj.created_by.first_name if obj.created_by else ""} {obj.created_by.last_name if obj.created_by else ""}'


@admin.register(Territory)
class TerritoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'territory_type']