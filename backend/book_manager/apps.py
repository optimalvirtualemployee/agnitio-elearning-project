from django.apps import AppConfig


class BookManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'backend.book_manager'
