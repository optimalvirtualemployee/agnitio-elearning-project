from django.shortcuts import render, redirect
from django.views import View
from .models import Banners
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@admin_logged
@login_required
def banner_list(request):
    template_name = 'banner_list.html'
    banner_list = Banners.objects.all()
    return render(request, template_name, {'banner_list': banner_list, 'banner': True})


class BannerView(View):
    template_name = 'add_edit_banner.html'
    context = {'banner': True}

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        try:
            obj = Banners.objects.get(pk=pk)
        except Banners.DoesNotExist:
            obj = None
        self.context['obj'] = obj
        return render(request, self.template_name, self.context)

    def post(self, request, *args, **kwargs):
        try:
            title = request.POST.get('title')
            desc = request.POST.get('desc')
            banner_id = request.POST.get('banner_id')
            image = request.FILES.get('image')
            if banner_id:
                obj = Banners.objects.get(id=banner_id)
            else:
                obj = Banners()
            if image:
                fs = FileSystemStorage()
                fs.save(image.name, image)
                obj.image = image.name
            obj.title = title
            obj.short_description = desc
            obj.save()
            if banner_id:
                messages.success(request, 'Banner Updated successfully.')
            else:
                messages.success(request, 'Banner Created successfully.')
            return redirect('banner:banner_list')
        except Exception as e:
            messages.error(request, str(e))
            if banner_id:
                return redirect('banner:banner_edit')
            return redirect('banner:banner_add')

    @staticmethod
    def banner_delete(request, *args, **kwargs):
        pk = kwargs.get('pk')
        Banners.objects.get(pk=pk).delete()
        messages.success(request, 'Banner Deleted successfully.')
        return redirect('banner:banner_list')
