from django.urls import path, include
from .views import *

app_name = 'banner'

urlpatterns = [
    path('List/', banner_list, name="banner_list"),
    path('Create/', BannerView.as_view(), name="banner_add"),
    path('update/<int:pk>/', BannerView.as_view(), name="banner_edit"),
    path('delete/<int:pk>/', BannerView.banner_delete, name="banner_delete"),
]