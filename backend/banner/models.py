from django.db import models


class Banners(models.Model):
    title = models.CharField(max_length=244, default='')
    short_description = models.TextField(default='')
    image = models.ImageField()  # image

    def __str__(self):
        return self.title