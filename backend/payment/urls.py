from django.urls import path, include
from .views import *

app_name = 'payment'

urlpatterns = [
    path('payment_setting/', payment_setting, name="payment_setting"),
]
