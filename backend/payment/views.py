from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import PaymentSetting
from django.contrib import messages
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@admin_logged
@login_required
def payment_setting(request):
    if request.method == 'POST':
        payment_setting_type = request.POST.get('payment_setting_type')
        api_key = request.POST.get('api_key')
        secret_key = request.POST.get('secret_key')
        setting_obj = PaymentSetting.objects.create(config_type=payment_setting_type, api_key=api_key, secret_key=secret_key)
        if setting_obj:
            PaymentSetting.objects.all().exclude(id=setting_obj.id).delete()
            messages.success(request, "Payment setting created successfully!!", extra_tags='text-success')
    return render(request, 'payment_setting.html', {'payment_setting': True})

