from django import forms
from .models import Payment
from backend.book_manager.models import Book
from backend.user_manager.models import User



class Payment_Form(forms.ModelForm):
    book = forms.ModelChoiceField(required=True,queryset=Book.objects.all(),
                                  widget=forms.Select(attrs={'placeholder': "Status *", "class": "form-control",
                                                             "multiple":"true","name":"book[]"}))
    user = forms.ModelChoiceField(required=True,queryset=User.objects.filter(user_type=2),
                                  widget=forms.Select(attrs={'placeholder': "Status *", "class": "form-control",
                                                             "multiple":"true","name":"book[]"}))
    class Meta:
        model = Payment
        exclude = ['book','user']
