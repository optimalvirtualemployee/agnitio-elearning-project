from django.db import models
from backend.user_manager.models import User
from backend.book_manager.models import Book
import django


class Payment(models.Model):
    user = models.ManyToManyField(User, related_name='user_%(class)ss')
    payment_method = models.CharField(max_length=150, null=True, blank=True)
    book = models.ManyToManyField(Book,related_name='book_%(class)ss')
    order_id = models.CharField(max_length=150, null=True, blank=True)
    razorpay_order_id = models.CharField(max_length=150, null=True, blank=True)
    razorpay_payment_id = models.CharField(max_length=150, null=True, blank=True)
    razorpay_signature = models.CharField(max_length=150, null=True, blank=True)
    txn_id = models.CharField(max_length=150, null=True, blank=True)
    txn_status = models.CharField(max_length=150, null=True, blank=True)
    txn_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    price = models.CharField(max_length=150, null=True, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.order_id)


class PaymentSetting(models.Model):
    CONFIG_TYPE = ((0, 'Sandbox'), (1, 'Production'))
    config_type = models.IntegerField(default=0, choices=CONFIG_TYPE)
    api_key = models.CharField(max_length=255)
    secret_key = models.CharField(max_length=255)

    def __str__(self):
        return self.api_key


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='user_%(class)ss')
    book = models.ForeignKey(Book, on_delete=models.SET_NULL, null=True, blank=True, related_name='book_%(class)ss')
    price = models.CharField(max_length=150, null=True, blank=True)
    razorpay_order_id = models.CharField(max_length=150, null=True, blank=True)
    qty = models.IntegerField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'User: {self.created_at}'



class AddToCart(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='user_%(class)ss')
    book = models.ForeignKey(Book, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='book_%(class)ss')
    price = models.FloatField(null=True, blank=True, default=0)
    qty = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(default=django.utils.timezone.now)


    def __str__(self):
        return f'User: {self.user.get_full_name()} -- Book: {self.book.title}'


