from django.contrib import admin
from backend.payment.models import Payment, PaymentSetting, Order, AddToCart

admin.site.register(Payment)
admin.site.register(PaymentSetting)
admin.site.register(Order)


class AddToCartAdmin(admin.ModelAdmin):
    list_display = ('user','book','price','qty','created_at')
admin.site.register(AddToCart,AddToCartAdmin)
