from django import forms
from django.contrib.auth.forms import UserCreationForm, ReadOnlyPasswordHashField
from .models import User


class SignUpForm(UserCreationForm):
    USER_TYPE_CHOICES = (
        ('', '--- Select User Type ---'),
        (1, 'Student'),
        (2, 'Teacher'),
        (3, 'Principal'),
        (4, 'Admin'),
    )
    SEX = (
        ('', '--- Select Sex ---'),
        (1, 'Male'),
        (2, 'Female'),
        (3, 'Other'),
    )
    first_name = forms.CharField(max_length=30, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter first name','title': 'Please Enter User first name','required':'','autocomplete':'off',
               }), label=u'First name')
    last_name = forms.CharField(max_length=30, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter last name', 'title': 'Please Enter User first name','required':'','autocomplete':'off',
               }), label=u'Last name')
    email = forms.EmailField(max_length=254, required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Email','title': 'Please Enter User valid email ','required':'','autocomplete':'off',
                                        }), label=u'Email')
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','title': 'Please Enter User Strong Password ','required':'','autocomplete':'off'}), label=u'Password')
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','title': 'Please Use same password as previous entered ','required':'','autocomplete':'off'}), label=u'Confirmed Password',
                                )
    user_type = forms.ChoiceField(choices=USER_TYPE_CHOICES, required=True,
                             widget=forms.Select(
                                 attrs={'class': 'form-control',
                                        }))
    sex = forms.ChoiceField(choices=SEX, required=True, widget=forms.Select(
                                 attrs={'class': 'form-control',
                                        }))
    age = forms.IntegerField(required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Age','title': 'Please Enter User age in numbers only ','required':'','autocomplete':'off',
                                        }), label=u'Age')
    address = forms.CharField(max_length=255, required=True, widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Address', 'title': 'Please Enter User Address ','required':'','autocomplete':'off',
                                        }), label=u'Address')
    mobile = forms.CharField(max_length=150, required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Mobile', 'title': 'Please Enter User Mobile Number ','required':'','autocomplete':'off',
                                        }), label=u'Mobile')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2', 'user_type', 'sex',
                  'age', 'address', 'mobile')


class SignupChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()
    USER_TYPE_CHOICES = (
        ('', '--- Select User Type ---'),
        (1, 'Student'),
        (2, 'Teacher'),
        (3, 'Principal'),
        (4, 'Admin'),
    )
    SEX = (
        ('', '--- Select Sex ---'),
        (1, 'Male'),
        (2, 'Female'),
        (3, 'Other'),
    )
    first_name = forms.CharField(max_length=30, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter first name',
               }), label=u'First name')
    last_name = forms.CharField(max_length=30, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter last name',
               }), label=u'Last name')
    email = forms.EmailField(max_length=254, required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Email',
                                        }), label=u'Email')
    user_type = forms.ChoiceField(choices=USER_TYPE_CHOICES, required=True,
                                  widget=forms.Select(
                                      attrs={'class': 'form-control',
                                             }))
    sex = forms.ChoiceField(choices=SEX, required=True, widget=forms.Select(
        attrs={'class': 'form-control',
               }))
    age = forms.IntegerField(required=True,
                             widget=forms.NumberInput(
                                 attrs={'min': '1', 'class': 'form-control', 'placeholder': 'Enter Age',
                                        }), label=u'Age')
    address = forms.CharField(max_length=255, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Address',
               }), label=u'Address')
    mobile = forms.CharField(max_length=150, required=True,
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Mobile',
                                        }), label=u'Mobile')

    class Meta:
        model = User
        fields = ('password', 'first_name', 'last_name', 'email', 'user_type', 'sex',
                  'age', 'address', 'mobile')
        # fields = ['email', 'password', 'is_active', 'admin']

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
