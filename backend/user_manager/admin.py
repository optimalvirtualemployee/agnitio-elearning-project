from django.contrib import admin
from django.contrib.auth.models import Group
from .models import User, UserSession
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
from django.contrib.sessions.models import Session


@admin.register(User)
class UserModelAdmin(admin.ModelAdmin):
    list_display = ['email', 'is_superuser', 'is_staff']


admin.site.site_header = 'Agnito'
admin.site.unregister(Group)
admin.site.register(UserSession)


@receiver(user_logged_in)
def remove_other_sessions(sender, user, request, **kwargs):
    # remove other sessions
    Session.objects.filter(usersession__user=user).delete()

    # save current session
    request.session.save()

    # create a link from the user to the current session (for later removal)
    UserSession.objects.get_or_create(
        user=user,
        session_id=request.session.session_key
    )