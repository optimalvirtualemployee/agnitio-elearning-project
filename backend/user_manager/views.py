from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.views import View
from .forms import SignUpForm, SignupChangeForm
import csv
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from backend.user_manager.models import User
from django.contrib.auth.decorators import login_required
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator


@admin_logged
@login_required
def user_list(request):
    user_list = User.objects.all().exclude(is_superuser=True)
    print(user_list)
    return render(request, 'user_list.html', {'user_list': user_list, 'users': True})


@login_required
def profile(request):
    context = {}
    if request.method == 'POST':
        f_name = request.POST.get('first_name')
        l_name = request.POST.get('last_name')
        mobile = request.POST.get('mobile')
        id = request.POST.get('id')
        obj = User.objects.get(id=id)
        obj.first_name = f_name
        obj.last_name = l_name
        obj.mobile = mobile
        obj.save()
        context['user_obj'] = obj
        messages.success(request, 'Updated successfully.')
    obj = User.objects.get(id=request.user.id)
    context['user_obj'] = obj
    return render(request, 'profile.html', context)


@login_required
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('users:user_list')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form, 'users': True, 'msg': 'Create User'})


@login_required
def update_signup(request, pk):
    instance = get_object_or_404(User, id=pk)
    form = SignupChangeForm(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        return redirect('users:user_list')
    return render(request, 'edit_signup.html', {'form': form, 'pk': pk, 'users': True, 'msg': 'Update User'})


@login_required
def delete_user(request, *args, **kwargs):
    pk = kwargs.get('pk')
    User.objects.get(pk=pk).delete()
    messages.success(request, 'User Deleted successfully.')
    return redirect('users:user_list')


class UsersCreateView(View):
    """
    create users in bulk by CSV
    """
    template_name = 'create_users.html'
    context = {'users': True}

    def get(self, request):
        return render(request, self.template_name, self.context)

    def post(self, request):
        file = request.FILES['file']
        fs = FileSystemStorage()
        filename = fs.save(file.name, file)
        with open('media/'+filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            users = []
            for row in csv_reader:
                users.append(User(user_type=row[3], email=row[0], username=row[4], first_name=row[1], last_name=row[2], password=row[5]))
            User.objects.bulk_create(users)
        return render(request, self.template_name)

@login_required
def change_password(request):

    if request.method == 'POST':
        old_password = request.POST.get("q_Old_Password")
        new_password = request.POST.get("q_new_Password")
        confirmed_new_password = request.POST.get("q_confirm_new_Password")

        if old_password and new_password and confirmed_new_password:        
            user = User.objects.get(email= request.user.email)
            if not user.check_password(old_password):
                messages.warning(request, "your old password is not correct!")
            else:
                if new_password != confirmed_new_password:
                    messages.warning(request, "your new password not match the confirm password !")
                
                elif len(new_password) < 8 or new_password.lower() == new_password or \
                        new_password.upper() == new_password or new_password.isalnum() or \
                        not any(i.isdigit() for i in new_password):
                    messages.warning(request, "your password is too weak!")

                else:
                    user.set_password(new_password)
                    user.save()
                    messages.success(request, "your password has been changed successfuly.!")
                    return redirect('dashboard_namespace:home')

        else:
            messages.warning(request, " sorry , all fields are required !")

    context = {
    }
    return render(request, "change_password.html", context)
