from django.urls import path
from .views import *

app_name = 'users'

urlpatterns = [
    path('List/', user_list, name="user_list"),
    path('profile/', profile, name="profile"),
    path('signup/', signup, name="signup"),
    path('Create/', UsersCreateView.as_view(), name="users_create"),
    path('update/<int:pk>/', update_signup, name="edit_user"),
    path('delete/<int:pk>/', delete_user, name="delete_user"),
    path('change_password/', change_password, name="change_password"),
]
