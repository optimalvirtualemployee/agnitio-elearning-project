from django.db import models

CAT_CHOICES = ((1, 'Subjects'), (2, 'Board'), (3, 'Classes'), (4, 'Bachelors'))


class Book_category(models.Model):
    cat = models.IntegerField(default=1, choices=CAT_CHOICES)
    name = models.CharField(max_length=100)
    parent_category = models.ForeignKey('self', default=None, on_delete=models.CASCADE, null=True,related_name='subcategories')
    show_on_ui = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        verbose_name = "Book Category"
        verbose_name_plural = "Book Categories"

    def __str__(self):
        return self.name
