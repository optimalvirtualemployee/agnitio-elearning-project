from django.urls import path
from .views import *

app_name = 'cat'

urlpatterns = [
    path('List/', category_list, name="category_list"),
    path('Add/', CategoryView.as_view(), name="add_category"),
    path('<int:pk>/update/', CategoryView.as_view(), name="edit_category"),
    path('<int:pk>/delete/', CategoryView.category_delete, name="category_delete"),
    path('get-subcategory/', get_subcategory, name="get_subcategory"),
]