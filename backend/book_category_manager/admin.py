from django.contrib import admin
from .models import Book_category
from django.utils.html import format_html


@admin.register(Book_category)
class Book_categoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'cat', 'action']
    list_display_links = ('action',)
    search_fields = ['name']

    def action(self, obj):
        return format_html(f'<a href="/admin/book_category_manager/book_category/{obj.id}/change/" class="btn btn-primary">View</a>')
