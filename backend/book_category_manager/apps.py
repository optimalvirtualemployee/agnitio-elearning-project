from django.apps import AppConfig


class BookCategoryManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'backend.book_category_manager'
