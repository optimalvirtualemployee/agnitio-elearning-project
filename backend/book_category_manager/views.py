from django.shortcuts import render, redirect
from django.views import View
import json
from .models import *
from django.contrib import messages
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse


@admin_logged
@login_required
def category_list(request):
    cat_list = Book_category.objects.all().order_by('-id')
    return render(request, 'category_list.html', {'cat_list': cat_list, 'category': True})

@admin_logged
@login_required
def get_subcategory(request):
    category_id_str = request.GET.get('category_ids')
    category_id_list = []
    if category_id_str:
        category_id_list = category_id_str.split(",")
        category_id_list = list(map(int, category_id_list))
    cat_list = Book_category.objects.filter(parent_category_id__in=category_id_list).order_by('-id')
    
    cat_list_append = []
    for cat in cat_list:
        _dict = {
            'id': cat.id,
            'name': cat.name,
            'parent_category_id': cat.parent_category_id
        }
        cat_list_append.append(_dict)
    
    return HttpResponse(json.dumps(cat_list_append))


class CategoryView(View):
    template_name = 'add_edit_category.html'
    context = {'category': True}

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        try:
            obj = Book_category.objects.get(pk=pk)
        except Book_category.DoesNotExist:
            obj = None
        self.context['obj'] = obj
        self.context['CAT_CHOICES'] = CAT_CHOICES
        self.context['category_list'] = Book_category.objects.all()
        return render(request, self.template_name, self.context)

    def post_backup(self, request):
        try:
            name = request.POST.get('name')
            cat = request.POST.get('cat')
            cat_id = request.POST.get('cat_id')
            if cat_id:
                obj = Book_category.objects.get(id=cat_id)
            else:
                obj = Book_category()
            obj.name = name
            obj.cat = cat
            obj.save()
            if cat_id:
                messages.success(request, 'Category Updated successfully.')
            else:
                messages.success(request, 'Category Created successfully.')
            return redirect('cat:category_list')
        except Exception as e:
            messages.error(request, str(e))
            if cat_id:
                return redirect('cat:edit_category')
            return redirect('cat:add_category')

    def post(self, request):
        try:
            name = request.POST.get('name')
            parent_category_id = request.POST.get('parent_category_id')
            show_on_ui = request.POST.get('show_on_ui')
            show_on_ui = True if show_on_ui else False
            cat_id = request.POST.get('cat_id')

            if cat_id:
                obj = Book_category.objects.get(id=cat_id)
            else:
                obj = Book_category()
            
            # update or add data
            obj.parent_category_id = parent_category_id
            obj.show_on_ui = show_on_ui
            obj.name = name
            obj.save()

            if cat_id:
                messages.success(request, 'Category Updated successfully.')
            else:
                messages.success(request, 'Category Created successfully.')

            return redirect('cat:category_list')
        except Exception as e:
            messages.error(request, str(e))
            if cat_id:
                return redirect('cat:edit_category')
            return redirect('cat:add_category')



    @staticmethod
    def category_delete(request, *args, **kwargs):
        pk = kwargs.get('pk')
        Book_category.objects.get(pk=pk).delete()
        messages.success(request, 'Category Deleted successfully.')
        return redirect('cat:category_list')

