from django.urls import path, include
from .views import *

app_name = 'contacts'

urlpatterns = [
    path('List/', ContactListView.as_view(), name="contact_list"),
]
