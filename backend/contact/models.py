from django.db import models

ENQUIRY_TYPE = ((1, 'Interview'), (2, 'Sale'))


class ContactUs(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    mobile = models.CharField(max_length=150)
    enquiry_type = models.IntegerField(choices=ENQUIRY_TYPE)
    email = models.CharField(max_length=150)
    company_name = models.CharField(max_length=150, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'
