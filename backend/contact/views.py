from django.shortcuts import render
from .models import ContactUs
from django.views import View
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class ContactListView(View):

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        template_name = 'contact_list.html'
        contact_list = ContactUs.objects.all()
        context = {'contact_list': contact_list, 'contact': True}
        return render(request, template_name, context)


