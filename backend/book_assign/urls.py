from django.urls import path, include
from .views import *

app_name = 'book_assign'

urlpatterns = [
    path('List/', book_assign_list, name="book_assign_list"),
    path('Assign/', book_assign, name="book_assign"),
    path('<int:pk>/delete/',book_assign_delete, name="book_assign_delete"),
    path('<int:pk>/edit/',book_assign_edit, name="book_assign_edit"),
]
