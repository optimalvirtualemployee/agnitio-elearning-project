from django.shortcuts import render, redirect, get_object_or_404
from backend.payment.models import Payment
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from backend.payment.forms import Payment_Form
from django.contrib.auth.decorators import login_required
from backend.book_manager.models import Book
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.views import generic
from backend.user_manager.models import User



@admin_logged
@login_required
def book_assign_list(request):
    book_assign_list = Payment.objects.filter(payment_method='assign')
    return render(request, 'book_assign_list.html', {'book_assign_list': book_assign_list, 'book_assign': True})

@admin_logged
@login_required
def book_assign(request):
    book_list = Book.objects.all().values('id', 'title')
    teacher_list = User.objects.filter(user_type=2).values('id', 'first_name', 'last_name')
    payment_form = Payment_Form()
    if request.method == 'POST':
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')
        payment_form = Payment_Form(request.POST)
        if payment_form.is_valid():
            data = payment_form.save(commit=False)
            data.end_date = end_date
            data.start_date = start_date
            data.payment_method = 'assign'
            data.txn_status = 'Success'
            data.save()
            book = request.POST.getlist('book')
            users = request.POST.getlist('user')
            data.book.set(book)
            data.user.set(users)
            payment_form.save_m2m()
        return redirect('book_assign:book_assign_list')
    else:
        payment_form = Payment_Form()
    return render(request, 'book_assign.html', {'payment_form':payment_form,'teacher_list': teacher_list, 'book_assign': True, 'book_list': book_list})


@admin_logged
@login_required
def book_assign_delete(request,pk):
    Payment.objects.get(pk=pk).delete()
    messages.success(request, 'book assign Deleted successfully.')
    return redirect('book_assign:book_assign_list')

@admin_logged
@login_required
def book_assign_edit(request,pk):
    if Payment.objects.filter(pk=pk).exists():
        data = Payment.objects.get(id=pk)
        payment_form = Payment_Form(instance=data)
        if request.method == 'POST':
            start_date = request.POST.get('start_date')
            end_date = request.POST.get('end_date')
            payment_form = Payment_Form(request.POST,instance = data)
            if payment_form.is_valid():
                data = payment_form.save(commit=False)
                data.end_date = end_date
                data.start_date = start_date
                data.payment_method = 'assign'
                data.txn_status = 'Success'
                data.save()
                book = request.POST.getlist('book')
                users = request.POST.getlist('user')
                data.book.set(book)
                data.user.set(users)
                payment_form.save_m2m()
                messages.success(request, 'book assign updated.')
            return redirect('book_assign:book_assign_list')
    else:
        messages.error(request, 'pk is invalide.')
        return redirect('book_assign:book_assign_list')
    return render(request, 'book_assign.html',
                  {'payment_form': payment_form, 'book_assign': True,'data':data})
