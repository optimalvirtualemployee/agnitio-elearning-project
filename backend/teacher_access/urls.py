from django.urls import path
from .views import *

app_name = 'teacher'

urlpatterns = [
    path('access/', BookAccessView.as_view(), name="access"),
    path('timing/', BookTimingView.as_view(), name="timing"),
]