from django.shortcuts import render
from django.views import View
from backend.book_manager.models import Book
from backend.user_manager.models import User
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


class BookAccessView(View):
    template_name = 'book_access_management.html'

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request):
        book_list = Book.objects.all().values('id', 'title')
        return render(request, self.template_name, {'book_list': book_list})


class BookTimingView(View):
    template_name = 'book_timing_management.html'

    @method_decorator(admin_logged)
    @method_decorator(login_required)
    def get(self, request):
        return render(request, self.template_name)