from django.shortcuts import render
from backend.payment.models import Payment
from utilities.utilities import admin_logged
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@admin_logged
@login_required
def report_list(request):
    report_list = Payment.objects.all().exclude(payment_method='assign')
    return render(request, 'report_list.html', {'report_list': report_list, 'report': True})
