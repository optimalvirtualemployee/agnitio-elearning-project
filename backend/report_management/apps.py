from django.apps import AppConfig


class ReportManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'backend.report_management'
