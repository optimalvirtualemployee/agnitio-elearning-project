from django.urls import path, include
from .views import *

app_name = 'report'

urlpatterns = [
    path('List/', report_list, name="report_list"),
]