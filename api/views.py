from django.contrib.auth import tokens
from django.core.mail.message import EmailMessage
from django.template.context_processors import request
from rest_framework.response import Response
from rest_framework import generics, serializers, status, views, permissions
from .serializers import (ForgetPasswordSerializer, LoginSerializer, LibrarySerializer, ContactSerializer,
                          BannerSerializer, CategorySerializer, BookDocumentSerializer, PasswordResetCompleteSerializer,
                          SignupSerializer, BookDetailSerializer, NoteContentSerializer)
from rest_framework import generics
from rest_framework.views import APIView
from django.template.loader import render_to_string
from frontend.models import Wishlist
from .serializers import WishlistSerializer, AddToCartSerializer
from backend.book_manager.documents import BookDocument
from backend.user_manager.models import User
from backend.book_manager.models import Book, NoteContent, NoteData
from backend.book_category_manager.models import Book_category
from backend.contact.models import ContactUs
from backend.banner.models import Banners
from backend.payment.models import Payment
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from backend.payment.models import Payment, AddToCart
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    CompoundSearchFilterBackend
)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
from django.core.serializers import serialize
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
)
from backend.payment.models import Order
from django.db.models import Q

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_bytes, smart_str,force_str, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode,urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
import datetime as dt
from tzlocal import get_localzone
from django.shortcuts  import get_object_or_404
import json
import email.message
import smtplib
from django.core.mail import send_mail


class BannerView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            queryset = Banners.objects.all()
            pk = kwargs.get('pk')
            if pk is not None:
                queryset = queryset.filter(pk=pk).first()
                serializer = BannerSerializer(queryset)
            else:
                    serializer = BannerSerializer(queryset, many=True)
            return Response({"status": status.HTTP_200_OK, "msg": "Listing of banners", "data": serializer.data})
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class BookDetailView(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):
        try:
            queryset = Book.objects.filter(is_active=True).filter(~Q(epub_file=""))
            pk = kwargs.get('pk')
            if pk is not None:
                queryset = queryset.filter(pk=pk).filter(~Q(epub_file="")).first()
                serializer = BookDetailSerializer(queryset)
                cat = Book_category.objects.filter(pk=pk).first()
                if queryset:
                    resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                        "cat": cat.name if cat else None, "library": serializer.data}
                else:
                    resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                            "cat": cat.name if cat else None, "library": {}}
            else:
                serializer = BookDetailSerializer(queryset, many=True)
                if queryset:
                    resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                            "library": serializer.data}
                else:
                    resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                            "library": {}}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class WishlistView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    context = {}

    def get(self, request, *args, **kwargs):
        try:
            user_id = kwargs.get('user_id')
            queryset = Wishlist.objects.filter(user=user_id)
            # pk = kwargs.get('pk')

            serializer = WishlistSerializer(queryset, many=True)
            resp = {"status": status.HTTP_200_OK, "msg": "Listing of Wishlist",
                    "data": serializer.data}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})

    def post(self, request):
        try:
            user_id = request.data.get('user_id')
            queryset = Wishlist.objects.filter(user=user_id)
            serializer = WishlistSerializer(queryset, many=True)
            Wishlist.objects.update_or_create(
                user_id=user_id,
                book_id=request.data.get('book_id'))
            self.context['status'] = status.HTTP_200_OK
            self.context['msg'] = "Listing of Wishlist"
            self.context['data'] = serializer.data
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = str(e)
            self.context['data'] = {}
        return Response(self.context)


from django.http import JsonResponse
def wishlist_delete(request, pk):
    context = {}
    try:
        Wishlist.objects.get(id=pk).delete()
        context['status'] = status.HTTP_200_OK
        context['msg'] = 'Successfully Deleted'
    except Wishlist.DoesNotExist:
        context['status'] = status.HTTP_404_NOT_FOUND
        context['msg'] = 'Id Does not exist'
    return JsonResponse(context)


class SaveNote(APIView):
    serializer_class = NoteContentSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    context = {}

    def get(self, request):
        try:
            header = JWTAuthentication.get_header(self, request)
            het_row_token = JWTAuthentication.get_raw_token(self, header)
            get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
            get_user = JWTAuthentication.get_user(self, get_validated_token)
            user_id = get_user.id
            if request.GET['book_id']:
                book_id = request.GET['book_id']
                queryset = NoteContent.objects.filter(user_id=user_id).filter(book_id=book_id)
                serializer = NoteContentSerializer(queryset, many=True)
                resp = {"status": status.HTTP_200_OK, "message": "all records fetched successfully",
                        "book_id": book_id,
                        "user_id": user_id,
                       "notes": serializer.data}
                return Response(resp)
            else:
                return Response({"status": status.HTTP_404_NOT_FOUND, "message": "book_id is required."})
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "message": str(e)})



    def post(self, request):
        header = JWTAuthentication.get_header(self,request)
        het_row_token = JWTAuthentication.get_raw_token(self,header)
        get_validated_token = JWTAuthentication.get_validated_token(self,het_row_token)
        get_user = JWTAuthentication.get_user(self,get_validated_token)
        user_id = get_user.id
        try:
            note_id = request.data.get('note_id')
            note_content = request.data.get('note_content')
            book_id = request.data.get('book_id')
            cfiRange = request.data.get('cfiRange')
            chpaterName = request.data.get('chpaterName')
            color = request.data.get('color')
            content = request.data.get('content')
            key = request.data.get('key')
            pageNum = request.data.get('pageNum')
            paragraphCfi = request.data.get('paragraphCfi')
            if Book.objects.filter(id=book_id).exists():
                book_obj = Book.objects.get(id=book_id)
                if NoteContent.objects.filter(key=key).filter(user_id=user_id).exists():
                    obj = NoteContent.objects.get(key=key)
                else:
                    obj = NoteContent.objects.create(user_id=user_id, note_id=note_id,content=content,
                                               book_id=book_obj, cfiRange=cfiRange, chpaterName=chpaterName,color=color,
                                               key=key,pageNum=pageNum,paragraphCfi=paragraphCfi)
                NoteData_obj = NoteData.objects.create(key=obj, note_content=note_content)
                self.context['status'] = status.HTTP_200_OK
                self.context['msg'] = "Your note has been saved successfully."
                self.context['id'] = obj.id
                self.context['content_id'] = NoteData_obj.id
            else:
                self.context['status'] = status.HTTP_404_NOT_FOUND
                self.context['msg'] = 'Book id is incorrect.'
        except Exception as e:
            print(e)
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = 'Something went wrong'
        return Response(self.context)

    def delete(self, request):
        try:
            header = JWTAuthentication.get_header(self, request)
            het_row_token = JWTAuthentication.get_raw_token(self, header)
            get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
            get_user = JWTAuthentication.get_user(self, get_validated_token)
            user_id = get_user.id
            if request.GET['note_id']:
                id = request.GET['note_id']
                delete_status = NoteData.objects.filter(id=id).delete()
                self.context['status'] = status.HTTP_200_OK
                self.context['msg'] = 'remove successfully'
            else:
                self.context['status'] = status.HTTP_404_NOT_FOUND
                self.context['msg'] = 'note_id is required.'
        except:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = 'Something went wrong'
        return Response(self.context)

class BookDetail(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            queryset = Book.objects.filter(is_active=True)
            pk = kwargs.get('pk')
            if pk is not None:

                header = JWTAuthentication.get_header(self, request)
                het_row_token = JWTAuthentication.get_raw_token(self, header)
                get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
                get_user = JWTAuthentication.get_user(self, get_validated_token)
                user_id = get_user.id
                queryset = queryset.filter(pk=pk).first()
                current_date = dt.datetime.now()
                if get_user.user_type == 2:
                    if Payment.objects.filter(user=user_id).filter(book=queryset.id).\
                            filter(Q(start_date__lte=current_date), Q(end_date__gte=current_date)).exists():
                        serializer = BookDetailSerializer(queryset)
                        cat = Book_category.objects.filter(pk=pk).first()
                        resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                                "cat": cat.name if cat else None, "library": serializer.data}
                    else:
                        return Response({"status": status.HTTP_404_NOT_FOUND, "msg": "You are not authorize to access this book."})
                else:
                    if Payment.objects.filter(user=user_id).filter(book=queryset.id).exists():
                        serializer = BookDetailSerializer(queryset)
                        cat = Book_category.objects.filter(pk=pk).first()
                        resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                                "cat": cat.name if cat else None, "library": serializer.data}
                    else:
                        return Response({"status": status.HTTP_404_NOT_FOUND, "msg": "You are not authorize to access this book."})
            else:
                serializer = BookDetailSerializer(queryset, many=True)
                resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                        "library": serializer.data}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class PopularView(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):
        try:
            queryset = Book.objects.filter(popular=1, is_active=True).filter(~Q(epub_file=""))

            serializer = LibrarySerializer(queryset, many=True)
            resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                    "popular": serializer.data}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class LibraryView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            order_type = ""
            order_by = "title"
            user_id = kwargs.get('user_id', None)
            if "order_type" in request.GET:
                if request.GET["order_type"] == "DESC":
                    order_type = "-"
            if "order_by" in request.GET:
                order_by = request.GET["order_by"]
            set_order_filter = order_type+order_by
            if user_id is None:
                queryset = Book.objects.filter(is_active=True).order_by(set_order_filter)
                serializer = LibrarySerializer(queryset, many=True)
                resp = {"status": status.HTTP_200_OK, "msg": "Listing of books",
                        "library": serializer.data}
            else:
                pay_user = Payment.objects.filter(user=user_id).filter(txn_status='Success')
                queryset = Book.objects.prefetch_related('book_payments').filter(book_payments__in=pay_user,
                                                                                 is_active=True).order_by(set_order_filter)
                serializer = LibrarySerializer(queryset, many=True)
                resp = {"status": status.HTTP_200_OK, "msg": "Listing of books by user",
                        "library": serializer.data}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


def payment_save(request, payment_id, razorpay_order_id, signature, txn_status, user_id, books_data,pric,order_id):
    try:
        payment = Payment()
        payment.price = pric
        payment.razorpay_payment_id = payment_id
        payment.razorpay_order_id = razorpay_order_id
        payment.razorpay_signature = signature
        payment.payment_method = 'RazorPay'
        payment.txn_status = txn_status
        payment.order_id = order_id.id
        payment.save()
        payment.user.add(user_id)
        for item in books_data:
            payment.book.add(item)
        return True
    except Exception as e:
        print(e)
        return False


class OrderCart(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]


    def get(self, request, *args, **kwargs):
        messge = "get cart product"
        data = {}
        # AddToCartSerializer
        header = JWTAuthentication.get_header(self, request)
        het_row_token = JWTAuthentication.get_raw_token(self, header)
        get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
        get_user = JWTAuthentication.get_user(self, get_validated_token)
        if AddToCart.objects.filter(user=get_user).exists():
            queryset = AddToCart.objects.filter(user=get_user)
            serializer = AddToCartSerializer(queryset, many=True)
            data = serializer.data
        return Response({"status": status.HTTP_200_OK, "msg": messge,"data":data})

    def post(self, request, *args, **kwargs):
        messge = "product add to card successfully"
        header = JWTAuthentication.get_header(self, request)
        het_row_token = JWTAuthentication.get_raw_token(self, header)
        get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
        get_user = JWTAuthentication.get_user(self, get_validated_token)
        book_id = request.data.get('book_id')
        if not AddToCart.objects.filter(user=get_user).filter(book__id=book_id).exists():
            if Book.objects.filter(id=book_id).exists():
                book = get_object_or_404(Book,id=book_id)
                add_to_cart = AddToCart(user=get_user,book=book,price=book.price_in_inr,qty=1)
                add_to_cart.save()
            else:
                messge = "book_id is incorrect"
                return Response({"status": status.HTTP_404_NOT_FOUND, "msg": messge})
        return Response({"status": status.HTTP_200_OK, "msg": messge})

    def delete(self, request, *args, **kwargs):
        messge = "Product remove successfully."
        if request.GET['id']:
            AddToCart.objects.filter(id=request.GET['id']).delete()
        return Response({"status": status.HTTP_200_OK, "msg": messge})

class PaymentView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            header = JWTAuthentication.get_header(self, request)
            het_row_token = JWTAuthentication.get_raw_token(self, header)
            get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
            get_user = JWTAuthentication.get_user(self, get_validated_token)

            payment_id = request.data.get('payment_id')
            razorpay_order_id = request.data.get('razorpay_order_id')
            signature = request.data.get('signature')
            payment_status = request.data.get('status')
            user_id = request.data.get('user_id')
            order_detail = request.data.get('order_detail')
            qty = 0
            pric = 0
            books_data = []
            order = Order(razorpay_order_id=razorpay_order_id,user=get_user)
            order.save()
            for data in order_detail:
                qty = qty + int(data["qty"])
                pric = pric + float(data["price"])
                books_data.append(data["book_id"])
            # order_list = Order.objects.filter(user=user_id)
            res = payment_save(request, payment_id, razorpay_order_id, signature, payment_status, user_id, books_data,pric,order)
            Order.objects.filter(id=order.id).update(price=pric, qty=qty, is_deleted=True)
            if res:
                if payment_status == 'Success':
                    Order.objects.filter(user=user_id, is_deleted=False).update(is_deleted=True)
                    Wishlist.objects.filter(user=user_id).delete()
                    AddToCart.objects.filter(user=user_id).delete()
                return Response({"status": status.HTTP_200_OK, "msg": "Success"})
            else:
                return Response({"status": status.HTTP_404_NOT_FOUND, "msg": "Something went wrong"})
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class CartView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            cartId = kwargs.get('cartId')
            cartId = eval(cartId)
            queryset = Book.objects.filter(id__in=cartId, is_active=True)
            serializer = LibrarySerializer(queryset, many=True)
            return Response({"status": status.HTTP_200_OK, "msg": "Listing of cart items", "cart_items": serializer.data})
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class BookListingView(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):
        try:
            queryset = Book.objects.filter(is_active=True).filter(~Q(epub_file=""))
            f = int(request.GET.get('f', 1))
            tz = get_localzone()
            car_name= ""
            price_for_filter = 'price_in_inr'
            # if str(tz) == "Asia/Calcutta":
            #     price_for_filter = 'price_in_inr'
            # else:
            #     price_for_filter = 'price_by_currency'
            if f == 1:
                sort_val = 'created_at'
            elif f == 2:
                sort_val = '-created_at'
            elif f == 3:
                sort_val = f'-{price_for_filter}'
            else:
                sort_val = price_for_filter
            Wishlist.objects.filter(user_id=request.user.id).delete()
            queryset = Book.objects.filter(is_active=True).filter(~Q(epub_file="")).order_by(sort_val)
            if "pk" in request.GET:
                pk = request.GET.get('pk')
                if pk != '0':
                    queryset = queryset.filter(category__id=pk)
                    if Book_category.objects.filter(pk=pk).exists():
                        cat = Book_category.objects.filter(pk=pk).first()
                        car_name = cat.name
                serializer = LibrarySerializer(queryset, many=True)
            else:
                serializer = LibrarySerializer(queryset, many=True)
            return Response({"status": status.HTTP_200_OK, "msg": "Listing of books",
                             "cat": car_name,
                             "listing": serializer.data,
                             })
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "msg": str(e)})


class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    authentication_classes = []
    permission_classes = []
    context = {}

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.context['status'] = status.HTTP_200_OK
            self.context['msg'] = "Login successfully."
            self.context['email'] = serializer.data.get('email')
            self.context['id'] = serializer.data.get('id')
            self.context['user_type'] = serializer.data.get('user_type')
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = str(e)
            self.context['email'] = None
        return Response(self.context)


class SocialSignupView(APIView):
    serializer_class = SignupSerializer
    permission_classes = (AllowAny,)
    context = {}

    def post(self, request):
        try:
            email = request.data['email']
            if User.objects.filter(email=email).exists():
                user_obj = User.objects.filter(email=email).first()
                token = TokenObtainPairSerializer.get_token(user_obj)
                self.context['status'] = status.HTTP_200_OK
                self.context['msg'] = "This email id already exist."
                self.context['refresh'] = str(token)
                self.context['access_token'] = str(token.access_token)
                self.context['data'] = {'id': user_obj.id, 'first_name': user_obj.first_name, 'email': user_obj.email,
                                        'last_name': user_obj.last_name, 'user_type': user_obj.user_type,
                                        'sex': user_obj.sex, 'age': user_obj.age, 'address': user_obj.address,
                                        'mobile': user_obj.mobile, 'access': user_obj.access}
            else:
                serializer = self.serializer_class(data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                user_obj = User.objects.filter(email=email).first()
                token = TokenObtainPairSerializer.get_token(user_obj)
                self.context['status'] = status.HTTP_201_CREATED
                self.context['msg'] = "Signup created successfully."
                self.context['refresh'] = str(token)
                self.context['access_token'] = str(token.access_token)
                self.context['data'] = serializer.data
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = str(e)
            self.context['data'] = ''
        return Response(self.context)

class ProfileView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    context = {}

    def get(self, request):
        try:
            header = JWTAuthentication.get_header(self, request)
            het_row_token = JWTAuthentication.get_raw_token(self, header)
            get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
            get_user = JWTAuthentication.get_user(self, get_validated_token)
            user_id = get_user.id
            user_obj = User.objects.filter(id=user_id).first()
            resp = {"status": status.HTTP_200_OK, "message": "Profile Info",
                    "data": {'id': user_obj.id, 'first_name': user_obj.first_name, 'last_name': user_obj.last_name,
                             'email': user_obj.email,'user_type': user_obj.user_type,'sex': user_obj.sex,
                             'age': user_obj.age, 'address': user_obj.address,'mobile': user_obj.mobile}}
            return Response(resp)
        except Exception as e:
            return Response({"status": status.HTTP_404_NOT_FOUND, "message": str(e)})

    def post(self, request):
        header = JWTAuthentication.get_header(self, request)
        het_row_token = JWTAuthentication.get_raw_token(self, header)
        get_validated_token = JWTAuthentication.get_validated_token(self, het_row_token)
        get_user = JWTAuthentication.get_user(self, get_validated_token)
        user_id = get_user.id
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        email = request.data.get('email')
        user_type = request.data.get('user_type')
        sex = request.data.get('sex')
        age = request.data.get('age')
        address = request.data.get('address')
        mobile = request.data.get('mobile')
        if User.objects.filter(email=email).filter(~Q(id=user_id)).exists():
            resp = {"status": status.HTTP_404_NOT_FOUND, "message": "This email is use by other user"}
        else:
            User.objects.filter(id=user_id).update(first_name=first_name,last_name=last_name,user_type=user_type,
                                                   sex=sex,age=age,address=address,mobile=mobile)
            resp = {"status": status.HTTP_200_OK, "message": "Profile Updated"}
        return Response(resp)

class SignupView(APIView):
    serializer_class = SignupSerializer
    permission_classes = (AllowAny,)
    context = {}

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
            self.context['status'] = status.HTTP_201_CREATED
            self.context['msg'] = "Signup created successfully."
            self.context['data'] = serializer.data
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = str(e)
            self.context['data'] = ''
        return Response(self.context)


class ContactView(APIView):
    serializer_class = ContactSerializer
    authentication_classes = []
    permission_classes = []
    context = {}

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
            self.context['status'] = status.HTTP_200_OK
            self.context['msg'] = "Contact created successfully."
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = serializer.errors
        return Response(self.context)


class CategoryView(APIView):
    serializer_class = CategorySerializer
    authentication_classes = []
    permission_classes = []
    context = {}

    def get(self, request):
        try:
            # subject_list = Book_category.objects.filter(cat=1).values('id', 'name')
            # board_list = Book_category.objects.filter(cat=2).values('id', 'name')
            # classes_list = Book_category.objects.filter(cat=3).values('id', 'name')
            # bachelors_list = Book_category.objects.filter(cat=4).values('id', 'name')
            # # serializer = self.serializer_class(data_list, many=True)
            # self.context['status'] = status.HTTP_200_OK
            # self.context['msg'] = "Category List."
            # self.context['subject_list'] = list(subject_list)
            # self.context['board_list'] = list(board_list)
            # self.context['classes_list'] = list(classes_list)
            # self.context['bachelors_list'] = list(bachelors_list)
            parent_category = Book_category.objects.filter(parent_category_id=None, show_on_ui=True).values('id',
                                                                                                            'name')
            for parent in parent_category:
                parent['child'] = Book_category.objects.filter(parent_category_id=parent['id'], show_on_ui=True).values(
                    'id', 'name')
                for child in parent['child']:
                    child['child'] = Book_category.objects.filter(parent_category_id=child['id'],
                                                                  show_on_ui=True).values('id', 'name')
            self.context['status'] = status.HTTP_200_OK
            self.context['msg'] = "Category List."
            self.context['parent_category'] = list(parent_category)
        except Exception as e:
            self.context['status'] = status.HTTP_404_NOT_FOUND
            self.context['msg'] = str(e)
            self.context['subject_list'] = {}
        return Response(self.context)


class BookDocumentView(DocumentViewSet):
    document = BookDocument
    serializer_class = BookDocumentSerializer
    authentication_classes = []
    permission_classes = []
    lookup_field = 'first_name'
    fielddata = True
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        CompoundSearchFilterBackend,
    ]

    search_fields = (
        'author_name',
        'title',
        'asin',
        'subject',
        'identifier_name',
        'keywords'
    )
    multi_match_search_fields = (
        'author_name',
        'title',
        'asin',
        'subject',
        'identifier_name',
        'keywords',
    )
    filter_fields = {
        'id': 'id',
        'author_name': 'author_name',
        'title': 'title',
        'asin': 'asin',
        'subject': 'subject',
        'identifier_name': 'identifier_name',
        'keywords': 'keywords',
    }
    ordering_fields = {
        'id': None,
    }
    ordering = ('id',)


class ForgetPasswordView(APIView):
    serializer_class = ForgetPasswordSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        email_user = request.data['email']
        if User.objects.filter(email = email_user).exists():
            user = User.objects.get(email=email_user)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            # cureent_site = get_current_site(request=request)
            # print(cureent_site)
            resetlink = f"{settings.URL_PATH}reset-password-confirm/{uidb64}/{token}"
            # absurl = cureent_site + resetlink
            mail_subject = "Password Reset link"
            current_site = get_current_site(request)
            # ============================================================
            message = render_to_string('reset_password_email_template.html', {
                'user': user,
                'domain': current_site.domain,
                'resetlink': resetlink
            })
            email_msg = EmailMessage(mail_subject, message, settings.EMAIL_HOST_USER, [user.email],
                                     reply_to=[settings.EMAIL_HOST_USER])
            email_msg.content_subtype = 'html'
            email_msg.send(fail_silently=False)
            # ================================================================
            return Response({'success':True,'email_body':"Password reset link has sent to your email!"},status=status.HTTP_200_OK)
        
        else:
            return Response({'success':False,'msg':'Inavlid Email'},status=status.HTTP_200_OK)

class PasswordResetConfirmView(APIView):

    def post(self,request):
        
        try:
            uidb64=request.data['uidb64']
            token=request.data['token']
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user,token):
                return Response({'success':False,'message':"Token is not valid"},status=status.HTTP_401_UNAUTHORIZED)

            return Response({'success':True,'message':'Credentials Valid','uidb64':uidb64, 'token':token},status=status.HTTP_200_OK)
        except DjangoUnicodeDecodeError as er:
            if not PasswordResetTokenGenerator().check_token(user,token):
                return Response({'success':False,'message':"Token is not valid"},status=status.HTTP_401_UNAUTHORIZED)



class ResetPasswordCompleteView(APIView):
    serializer_class = PasswordResetCompleteSerializer

    def patch(self, request):
        
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success':True,'message':'Password reset successfully'},status=status.HTTP_200_OK)
