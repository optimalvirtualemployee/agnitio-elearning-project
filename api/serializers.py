from django.contrib import auth
from django.contrib.auth import tokens
from django.core.validators import EmailValidator, MaxLengthValidator, MinLengthValidator
from django.db.models.fields import EmailField
from backend.user_manager.models import User
from rest_framework import serializers

from django_elasticsearch_dsl_drf.serializers import DocumentSerializer
from backend.book_manager.documents import *
from backend.book_manager.models import Book, NoteContent, NoteData

from backend.contact.models import ContactUs
from backend.book_category_manager.models import Book_category
from backend.banner.models import Banners
from backend.payment.models import AddToCart
from rest_framework.exceptions import AuthenticationFailed
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str,force_str, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode,urlsafe_base64_encode
from frontend.models import Wishlist


class ChoiceField(serializers.ChoiceField):
    def to_representation(self, obj):
        if obj == '' and self.allow_blank:
            return obj
        return self._choices[obj]

    def to_internal_value(self, data):
        # To support inserts with the value
        if data == '' and self.allow_blank:
            return ''

        for key, val in self._choices.items():
            if val == data:
                return key
        self.fail('invalid_choice', input=data)


class ContactSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[EmailValidator])
    mobile= serializers.CharField(validators=[MinLengthValidator(10),MaxLengthValidator(12)])
    first_name= serializers.CharField(validators=[MinLengthValidator(3)],trim_whitespace=True)
    last_name = serializers.CharField(validators=[MinLengthValidator(3)],trim_whitespace=True)
    # company_name = serializers.CharField(trim_whitespace=True,required=False)
    message = serializers.CharField(trim_whitespace=True)
    class Meta:
        model = ContactUs
        fields = '__all__'


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banners
        fields = '__all__'




class LoginSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, min_length=6, write_only=True)
    email = serializers.CharField(max_length=255, min_length=6)
    id = serializers.IntegerField(read_only=True)
    user_type = serializers.IntegerField(read_only=True)

    class Meta:
        model = User
        fields = ["id", "password", "email", 'user_type']

    def validate(self, attrs):
        email = attrs.get("email", "")
        password = attrs.get("password", "")
        user = auth.authenticate(email=email, password=password)
        if not user:
            raise AuthenticationFailed("Invalid credentials, try again")
        if not user.is_active:
            raise AuthenticationFailed("Account disabled, contact admin")
        return {"email": user.email, "id": user.id, 'user_type': user.user_type}
        return super().validate(attrs)


class SignupSerializer(serializers.ModelSerializer):
    USER_TYPE_CHOICES = (
        (1, 'Student'),
        (2, 'Teacher'),
        (3, 'Principal'),
        (4, 'Admin'),
    )
    SEX = ((1, 'Male'), (2, 'Female'), (3, 'Other'))

    user_type = serializers.ChoiceField(
                        choices=USER_TYPE_CHOICES)
    sex = serializers.ChoiceField(choices=SEX)
    # sex = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'password', 'user_type', 'sex', 'age', 'address', 'mobile', 'access')
        extra_kwargs = {'password': {'write_only': True}}
        write_only = ['id']

    def get_sex(self, obj):
        return obj.get_sex_display()

    def get_user_type(self, obj):
        return obj.get_user_type_display()

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.is_active = True
        user.save()
        return user


class CategorySerializer(serializers.ModelSerializer):
    cat = serializers.CharField(
        source='get_cat_display'
    )

    class Meta:
        model = Book_category
        fields = ['id', 'cat', 'name']


class NoteDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = NoteData
        fields = ('id','note_content')
        depth = 1

class ChildBookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ('id','title','cover_file')
        depth = 1



class ChildUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id','first_name')
        depth = 1


class NoteContentSerializer(serializers.ModelSerializer):
    note_content = NoteDataSerializer(many=True)
    book_id = ChildBookSerializer(many=False)
    user = ChildUserSerializer(many=False)
    class Meta:
        model = NoteContent
        fields = ('id','note_id', 'note_content','content','book_id','accessTime','cfiRange','chpaterName','color','createTime'
                  ,'key','pageNum','paragraphCfi','user')
        depth = 1


class AddToCartSerializer(serializers.ModelSerializer):
    book = ChildBookSerializer(many=False)
    user = ChildUserSerializer(many=False)

    class Meta:
        model = AddToCart
        fields = ('id','user','book','price','qty','created_at')
        depth = 1


class BookDetailSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True, read_only=False)

    class Meta:
        model = Book
        fields = '__all__'


class WishlistSerializer(serializers.ModelSerializer):
    # book = BookDetailSerializer(read_only=True, many=True)

    class Meta:
        model = Wishlist
        fields = ['id', 'book']
        write_only_fields = ['user']
        depth = 1


class LibrarySerializer(serializers.ModelSerializer):
    # category = CategorySerializer(read_only=True)

    class Meta:
        model = Book
        fields = ['id', 'title', 'cover_file', 'price_by_currency', 'price_in_inr', 'tax', 'territory_out','author_name']


class BookDocumentSerializer(DocumentSerializer):

    class Meta(object):
        """Meta options."""
        model = Book
        document = BookDocument
        fields = (
            'id',
            'author_name',
            'title',
            'asin',
            'subject',
            'identifier_name',
            'keywords',
        )

        def get_location(self, obj):
            """Represent location value."""
            try:
                return obj.location.to_dict()
            except:
                return {}

class ForgetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)

    class Meta:
        fields = ['email']

class PasswordResetCompleteSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=8,write_only=True)
    uidb64 = serializers.CharField(min_length=1,write_only=True)
    token = serializers.CharField(min_length=1,write_only=True)

    class Meta:
        fields=['password','uidb64','token']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            uidb64 = attrs.get('uidb64')
            token = attrs.get('token')

            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user,token):
                raise AuthenticationFailed('Reset link is invalid',401)
            else:
                user.set_password(password)
                user.save()
        except Exception as er:
            raise AuthenticationFailed('Reset link is invalid',401)

        return super().validate(attrs)

