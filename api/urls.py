from os import name
from django.urls import path, include
from .views import *

urlpatterns = [
    path('contact/', ContactView.as_view()),
    path('login/', LoginView.as_view()),
    path('signup/', SignupView.as_view()),
    path('profile/', ProfileView.as_view()),
    path('social_signup/', SocialSignupView.as_view()),
    path('library/<int:user_id>', LibraryView.as_view()),
    path('library/', LibraryView.as_view()),
    path('popular/', PopularView.as_view()),
    path('banner/', BannerView.as_view()),
    path('wishlist/<int:user_id>/', WishlistView.as_view()),
    path('wishlistAdd/', WishlistView.as_view()),
    path('wishlistDelete/<int:pk>/', wishlist_delete),
    path('category/', CategoryView.as_view()),
    path('payment/', PaymentView.as_view()),
    path('order_cart/', OrderCart.as_view()),
    path('book-detail/<int:pk>/', BookDetailView.as_view()),
    path('book_detail/<int:pk>/', BookDetail.as_view()),
    path('saveNote/', SaveNote.as_view()),
    path('cart-item/<str:cartId>/', CartView.as_view()),
    path('book-listing/<int:pk>/', BookListingView.as_view()),
    path('forget_password/', ForgetPasswordView.as_view(), name='forget-password'),
    path('reset-password-confirm/', PasswordResetConfirmView.as_view(), name='reset-password-confirm'),
    path('reset-password-complete/', ResetPasswordCompleteView.as_view(),name='reset-password-complete'),
    # path('check_user_login/<int:pk>/', BookListingView.as_view()),
]
