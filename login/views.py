from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout


def Login(request):
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)
            return redirect("superadmin")
        else:
            messages.info(request, "Username Or Password Incorrect")
    context = {}
    if request.user.is_authenticated:
        return redirect("superadmin")
    return render(request, "login.html", context)

