from django.urls import path
from .views import *

app_name = 'login'

urlpatterns = (
    path("", Login, name="admin_login"),
)