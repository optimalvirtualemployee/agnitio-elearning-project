from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect


def admin_logged(function):
    def wrap(request, *args, **kwargs):
        user_type = None
        if request.user.is_authenticated:
            user_type = request.user.user_type
        if user_type != 1:
            return function(request, *args, **kwargs)
        else:
            logout(request)
            return redirect('login:admin_login')
    return wrap