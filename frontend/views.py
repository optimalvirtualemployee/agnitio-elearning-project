import datetime
from inspect import signature
from django.core.exceptions import ValidationError
from django.dispatch.dispatcher import receiver
from django.http.response import HttpResponseRedirect
from django.views import View
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.http import HttpResponse, JsonResponse, request
from backend.user_manager.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from backend.payment.models import Payment
from backend.book_manager.models import Book
from backend.payment.models import AddToCart
from django.conf import settings
import requests
import datetime as dt
import json
import razorpay
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest
from backend.payment.models import Order
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text, smart_bytes, smart_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
# from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from allauth.account.signals import user_logged_in
from .models import Wishlist
from backend.book_category_manager.models import Book_category

razorpay_client = razorpay.Client(
    auth=(settings.RAZOR_KEY_ID, settings.RAZOR_KEY_SECRET)
)

# global amount
# amount = 400*100


def session_check(function):
    def wrap(request, *args, **kwargs):
        token = request.session.get('TOKEN', None)
        if token is not None:
            return function(request, *args, **kwargs)
        else:
            return redirect('frontend:home')
    return wrap


def student_user(function):
    def wrap(request, *args, **kwargs):
        user_type = None
        if request.user.is_authenticated:
            user_type = request.user.user_type
        if user_type == 1 or user_type == 2:
            return function(request, *args, **kwargs)
        else:
            logout(request)
            return redirect('frontend:home')
    return wrap


class ContactView(View):
    template_name = 'contact.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        email = request.POST.get('email')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        url = f'{settings.URL_PATH}api/contact/'
        data = requests.post(url=url, data=request.POST).json()
        if data.get('status') == 200:
            subject = 'Welcome in SCS Learning.'
            message = f'Dear {first_name},\n\nThanks for contacting us via contact form.\nOur team will get in touch soon\n\n\nThanks and Regards\nSCS Learning Team,'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [email, ]
            send_mail(subject, message, email_from, recipient_list)
            messages.success(request, 'Thanks for connect with us.')
        else:
            msg = data.get('msg')
            err_msg = ''
            for k,v in msg.items():
                err_msg = f"{k}: {v[0]}"
                messages.error(request,err_msg)
            return render(request, self.template_name,{"data": request.POST})
        return redirect('frontend:contact_us')


class AboutView(View):
    template_name = 'about.html'

    def get(self, request):
        return render(request, self.template_name)


class NewsListingView(View):
    template_name = 'news-listing.html'

    def get(self, request):
        return render(request, self.template_name)


class NewsDetailView(View):
    template_name = 'news-detail.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


def epub_reader(request, pk):
    context = {}
    url = f"{settings.URL_PATH}api/book-detail/{pk}/"
    resp = requests.get(url=url).json()
    if resp.get('status') == 200:
        context['book'] = resp.get('library')
    return render(request, 'epub.html', context)


def purchased_epub_reader(request, pk):
    context = {}
    url = f"{settings.URL_PATH}api/book-detail/{pk}/"
    resp = requests.get(url=url).json()
    token = ""
    if request.user.is_authenticated:
        token = request.session['TOKEN']
    context['token'] = token
    if resp.get('status') == 200:
        context['book'] = resp.get('library')
    return render(request, 'purchased_epub.html', context)


def del_wishlist(request):
    try:
        book_id = request.POST.get('book_id')
        obj = Wishlist.objects.filter(user_id=request.user.id, book_id=book_id).delete()
        if obj:
            return JsonResponse({'status': True})
        else:
            return JsonResponse({'status': False})
    except Exception as e:
        return JsonResponse({'status': False})



def get_cart_product(reqest):
    status= False
    data = []
    message = ""
    try:
        if AddToCart.objects.filter(user=reqest.user).exists():
            get_data = AddToCart.objects.filter(user=reqest.user)
            status = True
            if get_data:
                for item in get_data:
                    new_data = {}
                    new_data["id"] = item.id
                    new_data["user"] = item.user.id
                    new_data["book"] = item.book.id
                    new_data["price"] = item.price
                    new_data["qty"] = item.qty
                    new_data["created_at"] = item.created_at
                    data.append(new_data)
        message = "data get"
        return JsonResponse({'status': status,"data":data})
    except Exception as e:
        return JsonResponse({'status': status, "messge": str(e),"data":data})


def add_to_cart(request):
    book_id = request.POST.get('book_id')
    user = request.user
    status = True
    messge = "product already add"
    try:
        if not AddToCart.objects.filter(user=user).filter(book__id=book_id).exists():
            if Book.objects.filter(id=book_id).exists():
                book = get_object_or_404(Book,id=book_id)
                add_to_cart = AddToCart(user=user,book=book,price=book.price_in_inr,qty=1)
                add_to_cart.save()
                status = True
                messge = "product add to card"
            else:
                status = False
                messge = "book_id is incorrect"
        return JsonResponse({'status': status,"messge":messge})
    except Exception as e:
        status = False
        return JsonResponse({'status': status,"messge":str(e)})

def wishlist(request):
    try:
        book_id = request.POST.get('book_id')
        obj = Wishlist.objects.update_or_create(user_id=request.user.id, book_id=book_id)
        if obj:
            return JsonResponse({'status': True})
        else:
            return JsonResponse({'status': False})
    except Exception as e:
        return JsonResponse({'status': False})


class HomeView(View):
    template_name = 'index.html'
    context = {}

    def get(self, request):
        # messages.success("")
        if request.user.is_authenticated and request.user.user_type not in [1, 2]:
            logout(request)
        url = f'{settings.URL_PATH}api/popular/'
        resp = requests.get(url=url).json()
        if resp.get('status') == 200:
            book_list = resp.get('popular')
        else:
            book_list = []
        url1 = f'{settings.URL_PATH}api/book-listing/2/?pk=2'
        resp1 = requests.get(url=url1).json()
        if resp1.get('status') == 200:
            book_list_schools = resp1.get('listing')
        else:
            book_list_schools = []
        url2 = f'{settings.URL_PATH}api/book-listing/1/?pk=1'
        resp2 = requests.get(url=url2).json()
        if resp2.get('status') == 200:
            book_list_he = resp2.get('listing')
        else:
            book_list_he = []
        self.context['book_list'] = book_list
        self.context['book_list_schools'] = book_list_schools
        self.context['book_list_he'] = book_list_he
        return render(request, self.template_name, self.context)

def EmailTempate(request):
    current_site = get_current_site(request)
    mail_subject  = "Email Configuration"
    message = render_to_string('acc_active_email.html', {
        'user': "Deepak",
        'domain': current_site.domain,
        'uid': "isdjdvdfhvb",
        'token': "kdjsnjdhvbfv",
    })
    to_email = "deepaksinghpatel052@gmail.com"
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )


    return render(request,'payment_coformation_email_template.html',{"domain":'https://127.0.0.1:8000/'})

class SignupView(View):
    url = f'{settings.URL_PATH}api/signup/'
    auth_url = f'{settings.URL_PATH}api/token/'

    def post(self, request):
        username = request.POST.get('email')
        password = request.POST.get('password1')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        role = request.POST.get('role')
        age = request.POST.get('age')
        sex = request.POST.get('sex')
        address = request.POST.get('address')
        mobile = request.POST.get('mobile')
        payload = {'email': username, 'password': password,
                   'first_name': first_name, 'last_name': last_name,
                   'user_type': role, 'sex': sex, 'address': address,
                   'mobile': mobile, 'age': age}
        # payload = request.POST
        singup = requests.post(self.url, data=payload).json()
        if singup.get('status') == 201:
            user_id = singup['data']['email']
            user = User.objects.get(email=user_id)
            current_site = get_current_site(request)
            mail_subject = 'Activate your account.'
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(smart_bytes(user.pk)),
                'token':PasswordResetTokenGenerator().make_token(user),
            })
            to_email = username
            email_msg = EmailMessage(mail_subject, message, settings.EMAIL_HOST_USER, [to_email],
                                     reply_to=[settings.EMAIL_HOST_USER])
            email_msg.content_subtype = 'html'
            email_msg.send(fail_silently=False)
            # user = requests.post(self.auth_url, data={'email': username, 'password': password}).json()
            # if user:
            #     user_auth = authenticate(request, username=username, password=password)
            #     if user_auth is not None:
            #         login(request, user_auth)
            #     request.session['TOKEN'] = user.get('access')
            resp = {'status': True,"msg":"Please confirm your email address to complete the registration"}
        else:
            resp = {'status': False, 'msg': 'This email id is already exist'}
        return JsonResponse(resp)


class LoginView(View):
    url = f'{settings.URL_PATH}api/token/'
    login_url = f'{settings.URL_PATH}api/login/'

    def post(self, request):
        try:
            username = request.POST.get('email')
            password = request.POST.get('password')
            # user_type = request.POST.get('user_type')
            payload = {'email': username, 'password': password}
            user = requests.post(self.url, data=payload)
            user = user.json()
            res_data = {'status': True}
            if user.get('access'):
                payload = {'email': username, 'password': password}
                login_user = requests.post(self.login_url, data=payload).json()
                if login_user and (login_user.get('user_type') == 1 or login_user.get('user_type') == 2):
                    res_data['user_type'] = login_user.get('user_type')
                    user_auth = authenticate(request, username=username, password=password)
                    if user_auth is not None:
                        login(request, user_auth)
                else:
                    res_data['msg'] = 'You are not Student.'
                request.session['TOKEN'] = user.get('access')
                status = res_data
            else:
                detail = user.get('detail')
                status = {'status': False, 'msg': detail}
        except Exception as e:
            status = {'status': False, 'msg': str(e)}
        return JsonResponse(status)


class LibraryView(View):
    template_name = 'student-library.html'
    context = {}

    @method_decorator(student_user)
    def get(self, request):
        pay_obj = Payment.objects.filter(user=request.user.id).filter(txn_status='Success')
        self.context['library'] = pay_obj
        # self.context['current_date'] = current_date
        return render(request, self.template_name, self.context)


def logout_view(request):
    # del request.session['TOKEN']
    logout(request)
    return redirect('frontend:home')


class WishlistView(View):
    template_name = 'wishlist.html'
    context = {}

    @method_decorator(student_user)
    def get(self, request):
        try:
            book_id = Wishlist.objects.filter(user_id=request.user.id).values_list('book_id', flat=True)
            book_id = tuple(book_id)
            token = request.session['TOKEN']
            headers = {'Content-Type': 'application/json',
                       'Authorization': f'Bearer {token}'}
            url = f"{settings.URL_PATH}api/cart-item/{book_id}/"
            resp = requests.get(url=url, headers=headers).json()
            if resp.get('status') == 200:
                self.context['cart_items'] = resp.get('cart_items')
            return render(request, self.template_name, self.context)
        except Exception as e:
            self.context['cart_items'] = {}
            return render(request, self.template_name, self.context)


def remove_cart_product(request):
    status= False
    id = request.POST.get('id')
    data = []
    message = ""
    try:
        if AddToCart.objects.filter(user=request.user).filter(id =id).exists():
            AddToCart.objects.filter(user=request.user).filter(id =id).delete()
        message = "Product remove"
        status = True
        return JsonResponse({'status': status,"messge":message})
    except Exception as e:
        return JsonResponse({'status': status, "messge": str(e)})



class CartView(View):
    template_name = 'cart.html'
    context = {}

    @method_decorator(student_user)
    def get(self, request):
        self.context['cart_items'] = {}
        try:
            token = request.session['TOKEN']
            headers = {'Content-Type': 'application/json',
                       'Authorization': f'Bearer {token}'}
            if AddToCart.objects.filter(user=request.user).exists():
                get_data = AddToCart.objects.filter(user=request.user)
                self.context['cart_items'] = get_data
            return render(request, self.template_name, self.context)
        except Exception as e:
            return render(request, self.template_name, self.context)


def get_amount(request,order_id=0):
    tot_amount = 0
    if Order.objects.filter(id=order_id).exists():
        order_list = get_object_or_404(Order, id=order_id)
        tot_amount = float(order_list.price)
    return tot_amount * 100


def order_place(request):
    try:
        currency = 'INR'
        order_id = 0
        if request.GET["order_id"]:
            order_id = request.GET["order_id"]
        amount = get_amount(request,order_id)
        razorpay_order = razorpay_client.order.create(dict(amount=amount,
                                                           currency=currency,
                                                           payment_capture='0'))

        # order id of newly created order.
        razorpay_order_id = razorpay_order['id']
        callback_url = '/paymenthandler/'
        Order.objects.filter(id=order_id).update(razorpay_order_id=razorpay_order_id)
        # we need to pass these details to frontend.
        context = {}
        context['razorpay_order_id'] = razorpay_order_id
        context['razorpay_merchant_key'] = settings.RAZOR_KEY_ID
        context['razorpay_amount'] = amount
        context['currency'] = currency
        context['callback_url'] = callback_url

        return render(request, 'order_place.html', context=context)
    except Exception as e:
        return HttpResponse(str(e))


@csrf_exempt
def paymenthandler(request):
    # only accept POST request.
    if request.method == "POST":
        try:
            # get the required parameters from post request.
            payment_id = request.POST.get('razorpay_payment_id', '')
            razorpay_order_id = request.POST.get('razorpay_order_id', '')
            signature = request.POST.get('razorpay_signature', '')
            params_dict = {
                'razorpay_order_id': razorpay_order_id,
                'razorpay_payment_id': payment_id,
                'razorpay_signature': signature
            }
            # verify the payment signature.
            result = razorpay_client.utility.verify_payment_signature(params_dict)
            email_from = settings.EMAIL_HOST_USER

            result = 0
            # if result is None:
            # try:
            order_obj =  get_object_or_404(Order,razorpay_order_id=razorpay_order_id)
            amount = get_amount(request,order_obj.id)  # Rs. 200
            razorpay_client.payment.capture(payment_id, amount)
            payment_save(request, payment_id, razorpay_order_id, signature, 'Success')
            # render success page on successful caputre of payment

            if order_obj:
                subject = 'Payment Confirmation from SCS Learning.'
                current_site = get_current_site(request)
                message = render_to_string('payment_coformation_email_template.html', {
                    'user': request.user,
                    'domain': current_site.domain,
                })
                recipient_list = request.user.email
                email_msg = EmailMessage(subject, message, settings.EMAIL_HOST_USER, [recipient_list],
                                         reply_to=[settings.EMAIL_HOST_USER])
                email_msg.content_subtype = 'html'
                email_msg.send(fail_silently=False)
            Wishlist.objects.filter(user_id=request.user.id).delete()
            return render(request, 'paymentsuccess.html')
        except Exception as e:
            send_mail("Exception", signature +" Rezor message "+str(e), email_from, ['scssupport@optimalvirtualemployee.com',])

            payment_save(request, payment_id, razorpay_order_id, signature, 'Fail')
            # if there is an error while capturing payment.
            return render(request, 'paymentfail.html')
        # else:
        #     payment_save(request, payment_id, razorpay_order_id, signature, 'Fail')
        #     send_mail("result not found Razor", "Rezor message"+razorpay_order_id, email_from, ['aksheer@optimalvirtualemployee.com',])
        #     # if signature verification fails.
        #     return render(request, 'paymentfail.html')
        # except:
        #     # if we don't find the required parameters in POST data
        #     return HttpResponseBadRequest()
    else:
        # if other than POST request is made.
        return HttpResponseBadRequest()


def payment_save(request, payment_id, razorpay_order_id, signature, status):
    order_list = Order.objects.filter(user_id=request.user.id, razorpay_order_id=razorpay_order_id)
    tot_amount = 0
    try:
        for order in order_list:
            if Payment.objects.filter(order_id=order.id).exists():
                Payment.objects.filter(order_id=order.id).update(razorpay_payment_id=payment_id,
                                                                 razorpay_signature=signature,txn_status=status,
                                                                 payment_method='RazorPay')
            else:
                payment = Payment()
                tot_amount += float(order.price) * float(order.qty)
                payment.price = tot_amount
                payment.razorpay_payment_id = payment_id
                payment.razorpay_order_id = razorpay_order_id
                payment.razorpay_signature = signature
                payment.txn_status = status
                payment.payment_method = 'RazorPay'
                payment.order_id = order
                payment.save()
                payment.user.add(request.user)
                payment.book.add(order.book)
    except Exception as e:
        subject = 'Payment Confirmation from SCS Learning.'
        current_site = get_current_site(request)
        message = render_to_string('Payment_cancle.html', {
            'user': request.user,
            'domain': current_site.domain,
            'message': str(order_list[0].book.id) + " " + str(order_list[0].book.title)+"  "+str(e)
        })
        email_msg = EmailMessage(subject, message, settings.EMAIL_HOST_USER, [request.user.email],
                                 reply_to=[settings.EMAIL_HOST_USER])
        email_msg.content_subtype = 'html'
        email_msg.send(fail_silently=False)
    print('payment saved')


class BookDetailView(View):
    template_name = 'book-detail.html'
    context = {}

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        url = f"{settings.URL_PATH}api/book-detail/{pk}/"

        resp = requests.get(url=url).json()
        if resp.get('status') == 200:
            self.context['book'] = resp.get('library')
            categories = self.context['book'].get('category')
            cat_list = []
            for cat in categories:
                cat_list.append(cat['id'])
            board = Book_category.objects.filter(parent_category=8, pk__in=cat_list)
            subject = Book_category.objects.filter(parent_category=7, pk__in=cat_list)
            self.context['board'] = board
            self.context['subject'] = subject
            
            #Created New List based upon rating
            self.context['book']['no_of_reviews_for_front'] = self.context['book']['no_of_reviews']
            if self.context['book']['publication_date']:

                date_format = self.context['book']['publication_date'].split('-')
                x = datetime.datetime(int(date_format[0]), int(date_format[1]), int(date_format[2]))
                self.context['book']['publication_date'] = x.strftime("%d %b , %Y")


            if self.context['book']['review_date']:
                date_format = self.context['book']['review_date'].split('-')
                x = datetime.datetime(int(date_format[0]), int(date_format[1]), int(date_format[2]))
                self.context['book']['review_date'] =x.strftime("%d %b , %Y")

            no_of_reviews = []
            no_of_reviews_default = []
            if self.context['book']['no_of_reviews'] and int(self.context['book']['no_of_reviews']) != '':
                for i in range(int(self.context['book']['no_of_reviews'])):
                    no_of_reviews.append(i)

                for i in range(5):
                    no_of_reviews_default.append(i)

                self.context['book']['no_of_reviews'] =  no_of_reviews
                self.context['book']['no_of_reviews_default'] = no_of_reviews_default


            #print(self.context['book']['no_of_reviews'])

        pur_book = Payment.objects.filter(user=request.user.id).filter(txn_status='Success').values_list('book', flat=True)
        wishlist_obj = Wishlist.objects.filter(user_id=request.user.id).values_list('book', flat=True)
        self.context['pur_book'] = pur_book
        self.context['wishlist_obj'] = wishlist_obj
        return render(request, self.template_name, self.context)


class BuyBookDetailView(View):
    template_name = 'book-detail-after-login.html'
    context = {}

    @method_decorator(student_user)
    def get(self, request, *args, **kwargs):

        pk = kwargs.get('pk')
        token = request.session['TOKEN']
        headers = {'Content-Type': 'application/json',
                   'Authorization': f'Bearer {token}'}
        url = f"{settings.URL_PATH}api/book_detail/{pk}/"
        resp = requests.get(url=url, headers=headers).json()
        board = Book_category.objects.filter(parent_category=8)
        subject = Book_category.objects.filter(parent_category=7)
        if resp.get('status') == 200:
            self.context['book'] = resp.get('library')
            categories = self.context['book'].get('category')
            cat_list = []
            for cat in categories:
                cat_list.append(cat['id'])
            board = Book_category.objects.filter(parent_category=8, pk__in=cat_list)
            subject = Book_category.objects.filter(parent_category=7, pk__in=cat_list)
            self.context['board'] = board
            self.context['subject'] = subject
            
            # Created New List based upon rating
            self.context['book']['no_of_reviews_for_front'] = self.context['book']['no_of_reviews']

            if self.context['book']['publication_date']:

                date_format = self.context['book']['publication_date'].split('-')
                x = datetime.datetime(int(date_format[0]), int(date_format[1]), int(date_format[2]))
                self.context['book']['publication_date'] = x.strftime("%d %b , %Y")


            no_of_reviews = []
            no_of_reviews_default = []
            if self.context['book']['no_of_reviews'] and int(self.context['book']['no_of_reviews']) != '':
                for i in range(int(self.context['book']['no_of_reviews'])):
                    no_of_reviews.append(i)

                for i in range(5):
                    no_of_reviews_default.append(i)

                self.context['book']['no_of_reviews'] = no_of_reviews
        else:
            return redirect('frontend:book_detail',pk=pk)
        return render(request, self.template_name, self.context)


class BookListingView(View):
    template_name = 'book-listing.html'
    context = {}

    def get(self, request, *args, **kwargs):
        # token = request.session['TOKEN']
        #
        # headers = {'Content-Type': 'application/json',
        #            'Authorization': f'Bearer {token}'}
        FILTER_VAL = (('1', 'Oldest to Newest'), ('2', 'Newest to Oldest'), ('3', 'Price High to Low'), ('4', 'Price Low to High'))
        pk = "0"
        f = request.GET.get('f', '1')
        set_filter  = "?"
        if f:
            set_filter = set_filter+f"f={f}&"
            # url = f"{settings.URL_PATH}api/book-listing/?pk={pk}&f={f}"
        if "id" in request.GET:
            id = request.GET.get('id')
            self.context['id'] = id
            set_filter = set_filter + f"pk={id}&"
        else:
            self.context['id'] = '0'
        url = f"{settings.URL_PATH}api/book-listing/{pk}/{set_filter}"
        resp = requests.get(url=url).json()
        if resp.get('status') == 200:
            self.context['listing'] = resp.get('listing')
            self.context['cat'] = resp.get('cat')
            self.context['pk'] = pk
            self.context['FILTER_VAL'] = FILTER_VAL
            self.context['f'] = f

        return render(request, self.template_name, self.context)


class SearchBookView(View):
    def get(self, request):
        # token = request.session['TOKEN']
        #
        # headers = {'Content-Type': 'application/json',
        #            'Authorization': f'Bearer {token}'}
        search_val = request.GET.get('search_val')
        url = f"{settings.URL_PATH}search/?search={search_val}"
        resp = requests.get(url=url).json()
        title = []
        if resp:
            for i in resp:
                title.append(f"""<li><a href='/book-detail/{i.get('id')}/'>{i.get('title')}</a></li>""")
        return JsonResponse({'status': True, 'title': title})


def successpayment(request):
    return render(request, 'paymentsuccess.html')


def order(request):
    try:
        qty = 0
        pric = 0
        order_id = 0
        if AddToCart.objects.filter(user=request.user).exists():
            cart_product = AddToCart.objects.filter(user=request.user)
            order = Order(user_id=request.user.id,user=request.user)
            order.save()
            order_id = order.id
            payment = Payment()
            payment.order_id = order.id
            payment.save()
            payment.user.add(request.user.id)
            for i in cart_product:
                qty = qty + i.qty
                tax = (i.book.tax / 100) * i.price
                pric = pric + i.price + tax
                payment.book.add(i.book.id)
            payment.price = pric
            if pric == 0:
                payment.payment_method = 'Free'
                payment.txn_status = 'Success'
            else:
                payment.payment_method = ''
                payment.txn_status = ''
            payment.save()
            Order.objects.filter(id=order.id).update(price=pric,qty=qty,is_deleted=True)
            Wishlist.objects.filter(user_id=request.user.id).delete()
            AddToCart.objects.filter(user=request.user).delete()
            # return JsonResponse({'status': True, 'payment_type': 'free'})
        if pric == 0:
            return JsonResponse({'status': True, 'payment_type': 'free','order_id':order_id})
        else:
            return JsonResponse({'status': True, 'payment_type': 'paid','order_id':order_id})
    except Exception as e:
        return JsonResponse({'status': False, 'msg': str(e)})


class ForgetPasswordView(View):
    template_name = 'forget_password.html'
    context = {}
    forget_url = f'{settings.URL_PATH}api/forget_password/'

    def get(self,request):
        return render(request,self.template_name)

    def post(self,request):

        email = request.POST.get('email',None)
        
        if email is not None:
            payload = {'email':email}
            
            res = requests.post(self.forget_url,payload).json()
            if res.get('success') == True:
                messages.success(request, res.get('email_body'))
            elif res.get('success') == False:
                messages.error(request, res.get('msg'))

        return redirect('frontend:forget_password')

class ResetPasswordConfirmView(View):
    reset_template = 'reset_password.html'
    invalid_template = 'invalid_token.html'
    context = {}
    confirm_url = f'{settings.URL_PATH}api/reset-password-confirm/'
    

    def get(self,request,uidb64,token):
        self.context['uidb64'] = uidb64
        self.context['token'] = token
        payload = {'uidb64':uidb64,'token':token}
        res = requests.post(self.confirm_url,payload).json()
        if res.get('success') == True:
            return render(request,self.reset_template,self.context)
        if res.get('success') == False:
            messages.error(request, res.get('message'))
            return render(request,self.invalid_template)


class PasswordResetCompleteView(View):
    complete_url = f'{settings.URL_PATH}api/reset-password-complete/'
    template_name = 'reset_password.html' #'password_reset_complete.html'
    context = {}

    def get(self,request):
        self.context['uidb64'] = ''
        self.context['token'] = ''
        return render(request,self.template_name,self.context)

    def post(self,request):

        uidb64 = request.POST.get('uidb')
        token = request.POST.get('token')
        password = request.POST.get('new_pass')
        re_pass = request.POST.get('re_pass')


        if password is not None and (password == re_pass):
            payload = {'uidb64':uidb64,'token':token,'password': password}
            res = requests.patch(self.complete_url,payload).json()
            if res.get('password'):
                messages.error(request,"".join(res['password']))
            if res.get('success') == True:
                messages.success(request, res.get('message'))
                return redirect('frontend:forget_password')
            if res.get('success') == False:
                messages.error(request, res.get('message'))
        else:
            messages.error(request, "Password and Retype password are not matching.")
        return redirect('frontend:reset-password-confirm', uidb64=uidb64,token=token)


def activate(request, uidb64, token):
    try:
        uid = smart_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and PasswordResetTokenGenerator().check_token(user,token):
        user.is_active = True
        user.save()
        messages.success(request,'Thank you for your email confirmation. Now you can login your account.')
        return render(request,'email_confirmation.html')
    else:
        messages.error(request,'Activation link is invalid!')
        return render(request,'email_confirmation.html')

def email_confirmation_sent_view(request):
    return render(request,'email_confirmation_sent.html')

@receiver(user_logged_in)
def handle_user_login(sender, user, request, **kwargs):
    url = f'{settings.URL_PATH}api/token/'
    payload = {'email': user.email, 'password': '12345'}
    user = requests.post(url, data=payload)
    user = user.json()
    request.session['TOKEN'] = user.get('access')

    

class TermsView(View):
    template_name = 'terms_and_condition.html'

    def get(self, request):
        return render(request, self.template_name)



class ReturnpolicyView(View):
    template_name = 'return_policy.html'

    def get(self, request):
        return render(request, self.template_name)
