from django.db import models
from backend.user_manager.models import User
from backend.book_manager.models import Book


class Wishlist(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                             related_name='user_%(class)ss')
    book = models.ForeignKey(Book, on_delete=models.CASCADE, null=True, blank=True,
                             related_name='book_%(class)ss')

    def __str__(self):
        return self.user.first_name
