
 def upper_case(func):
     def wrapper(*args, **kwargs):
        return func(*args, **kwargs).lower()

     return wrapper