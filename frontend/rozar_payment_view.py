from django.shortcuts import render
import razorpay
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest

razorpay_client = razorpay.Client(
    auth=(settings.RAZOR_KEY_ID, settings.RAZOR_KEY_SECRET)
)

amount = 200*100


# def order_place(request):
#     currency = 'INR'
#     amount = 1000
#
#     razorpay_order = razorpay_client.order.create( dict(amount=amount, currency=currency, payment_capture='0') )
#
#     # order id of newly created order.
#     razorpay_order_id = razorpay_order['id']
#     callback_url = '/paymenthandler/'
#
#     # we need to pass these details to frontend.
#     context = {}
#     context['razorpay_order_id'] = razorpay_order_id
#     context['razorpay_merchant_key'] = settings.RAZOR_KEY_ID
#     context['razorpay_amount'] = amount
#     context['currency'] = currency
#     context['callback_url'] = callback_url
#     print(context,'kkkk')
#     return render(request, 'payment_razor.html', context=context)


