$(document).ready(function() {
  
  $('.megamenu-toggle').click(function(){
    $('.megamenu-list').toggle();
  });

$('.book-slider').slick({
        autoplay:false,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 5, 
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [
    {
      breakpoint: 992,
      settings: {
        arrows: true,
        slidesToShow: 3, 
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        slidesToShow: 2, 
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 575,
      settings: {
        arrows: true,
        slidesToShow: 2, 
        slidesToScroll: 1,
      arrows: false
      }
    }
  ]
    });
  
  
  $('.latest-update-slider').slick({
        autoplay:false,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 1, 
        slidesToScroll: 1
    });
  
  $('.view-book-slider').slick({
        autoplay:false,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 1, 
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
      nextArrow: '<button class="slide-arrow next-arrow"></button>',
        slidesToScroll: 1,
    responsive:[
      {
        breakpoint: 992,
        settings: {
        arrows: false,
        }
      },
    ]
    });
  
  $('.feedback-slider').slick({
        autoplay:false,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 3, 
        slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
      nextArrow: '<button class="slide-arrow next-arrow"></button>',
        cssEase: 'linear',
        responsive: [
    {
      breakpoint: 992,
      settings: {
        arrows: true,
        slidesToShow: 2, 
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 767,
      settings: {
        arrows: true,
        slidesToShow: 1, 
        slidesToScroll: 1
      }
    }
  ]
    });

  $(document).ready(function() {
      price_details()
  })

    function price_details() {
      var local_tz = $('#local_tz').val();
      var tot_book_price = 0;
      var book_price = 0;
      var qtyVal = 0;
      var shipping_price = 0;
      var price_with_qty = 0;
      var tax = $('.tax')
      var inputs = $('.book_price');
      var qty_inputs = $('.qtyValue');
      for(var i = 0; i < inputs.length; i++){
            book_price = parseInt($(inputs[i]).val())
            qtyVal = parseInt($(qty_inputs[i]).val())
            taxVal = parseInt($(tax[i]).val())
            tot_book_price += book_price * qtyVal
            price_with_qty = book_price * qtyVal
            shipping_price += (price_with_qty*taxVal)/100;
        }
        if (local_tz === 'Asia/Calcutta') {
            $('#Subtotal').html('&#8377; '+tot_book_price)
        } else {
            $('#Subtotal').text('$ '+tot_book_price)
        }
        if (local_tz === 'Asia/Calcutta') {
            $('#shipping_price').html('&#8377; '+shipping_price)
        } else {
            $('#shipping_price').text('$ '+shipping_price)
        }
        var tot_price = tot_book_price + shipping_price;
        if (local_tz === 'Asia/Calcutta') {
           $('.total-price').html('&#8377; '+tot_price)
        } else {
            $('.total-price').text('$ '+tot_price)
        }
        $('#total_price').val(tot_price);
    }

  var minVal = 1, maxVal = 20; // Set Max and Min values
  // Increase product quantity on cart page
  $(".increaseQty").on('click', function(){
      var local_tz = $('#local_tz').val();
      var $parentElm = $(this).parents(".qtySelector");
      $(this).addClass("clicked");
      setTimeout(function(){
        $(".clicked").removeClass("clicked");
      },100);
      var value = $parentElm.find(".qtyValue").val();
      if (value < maxVal) {
        value++;
      }
      $parentElm.find(".qtyValue").val(value);
            var price = $(this).parent().prev().prev('.book_price').val();
            var tot_price = price * value;
            if (local_tz === 'Asia/Calcutta') {
                $(this).parent().prev('.price').text('INR '+tot_price);
            } else {
                $(this).parent().prev('.price').text('$ '+tot_price);
            }
            price_details()
  });
  // Decrease product quantity on cart page
  $(".decreaseQty").on('click', function(){
      var local_tz = $('#local_tz').val();
      var $parentElm = $(this).parents(".qtySelector");
      $(this).addClass("clicked");
      setTimeout(function(){
        $(".clicked").removeClass("clicked");
      },100);
      var value = $parentElm.find(".qtyValue").val();
      if (value > 1) {
        value--;
      }
      $parentElm.find(".qtyValue").val(value);
      var price = $(this).parent().prev().prev('.book_price').val();
            var tot_price = price * value;
            if (local_tz === 'Asia/Calcutta') {
                $(this).parent().prev('.price').text('INR '+tot_price);
            } else {
                $(this).parent().prev('.price').text('$ '+tot_price);
            }
            price_details()
  });
  
});

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

(function() {
    'use strict';
    window.addEventListener('load', function() {
      var form = document.getElementById('needValidation');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();


(function() {
    'use strict';
    window.addEventListener('load', function() {
      var form = document.getElementById('signupForm');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();


(function() {
    'use strict';
    window.addEventListener('load', function() {
      var form = document.getElementById('signinForm');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();


  $('#signinForm').submit(function(e){
    e.preventDefault();
    var email = $('#email').val();
    if(!email){
        $('#email').next('span').show()
    } else {
           $('#email').next('span').hide()
    }
    var password = $('#password').val();
    if (!password){
           $('#password').next('span').show()
    } else {
           $('#password').next('span').hide()
    }
    const csrftoken = getCookie('csrftoken');
    $.ajax({
        url: '/login/',
        method: 'POST',
        data: {email, password},
        headers: {'X-CSRFToken': csrftoken},
         beforeSend: function() {
            $('#loader').removeClass('hidden')
        },
        success: function(res){
          console.log(res.user_type, 'res.user_type')
            if (res.status && (res.user_type == 1 || res.user_type == 2)) {
                location.href = '/';
            } else {
                $('#login-error').text(res.msg)
            }
        },
        complete: function(){
            $('#loader').addClass('hidden')
        },
        error: function(res) {
            console.log(res);
        }
    })
})

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

$('#signupForm').submit(function(e){

    e.preventDefault();
    var first_name = $('#id_first_name').val();
    var last_name = $('#id_last_name').val();
    var email = $('#id_email').val()
    var password = $('#id_password1').val();
    var age = $('#id_age').val();
    var mobile = $('#id_mobile').val();
    var address = $('#id_address').val();
    var data = $(this).serialize()
    console.log(data)
    const csrftoken = getCookie('csrftoken');
    var flag = true
    if(first_name == '' || first_name == undefined){
      flag = false
    }
    else if(last_name == '' || last_name == undefined){
      flag = false
    }
    else if(email == '' || email == undefined || isEmail(email) == false){
      flag = false
    }
    else if(password == '' || password == undefined){
      flag = false
    }
    else if(age == '' || age == undefined){
      flag = false
    }
    else if(mobile == '' || mobile == undefined){
      flag = false
    }
    else if(address == '' || address == undefined){
      flag = false
    }
    if(flag){
      $.ajax({
          url: '/signup/',
          method: 'POST',
          data: data,
          headers: {'X-CSRFToken': csrftoken},
          beforeSend: function() {
              $('#loader').removeClass('hidden')
          },
          success: function(res){
              if (res.status) {
                  // location.href = '/Library/';
                  location.href = '/email_confirmation_sent/';
              } else {
                  $('#signup-error').text(res.msg)
              }
          },
          complete: function(){
              $('#loader').addClass('hidden')
          },
          error: function(res) {
              console.log(res);
          }
      })
    }
    
})

document.addEventListener("keyup", function (e) {
  var keyCode = e.keyCode ? e.keyCode : e.which;
          if (keyCode == 44) {
              stopPrntScr();
          }
      });
function stopPrntScr() {
          console.log("Stop print screen")
          var inpFld = document.createElement("input");
          inpFld.setAttribute("value", ".");
          inpFld.setAttribute("width", "0");
          inpFld.style.height = "0px";
          inpFld.style.width = "0px";
          inpFld.style.border = "0px";
          document.body.appendChild(inpFld);
          inpFld.select();
          document.execCommand("copy");
          inpFld.remove(inpFld);
      }
     function AccessClipboardData() {
          try {
              window.clipboardData.setData('text', "Access   Restricted");
          } catch (err) {
          }
      }
      setInterval("AccessClipboardData()", 300);


function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
            }
        }
    }
    return cookieValue;
}

$('#sign_up_btn1').click(function(){
    $('.signup-modal').modal('show');
    $('.login-modal').modal('hide');
    $('body').css('z-index', '-9999')
})



$('.remove_wishlist').on('click', function(){
  var book_id = $(this).attr('book-id');
  const csrftoken = getCookie('csrftoken');
  $.ajax({
        url: '/del_wish_list/',
        method: 'POST',
        data: {book_id},
        headers: {'X-CSRFToken': csrftoken},
         beforeSend: function() {
            $('#loader').removeClass('hidden')
        },
        success: function(res){
            if (res.status) {
                window.location.reload();
            }
        }
    })
})


$('.wishlist').on('click', function(){
  var book_id = $(this).attr('book-id');
  const csrftoken = getCookie('csrftoken');
  $.ajax({
        url: '/wish_list/',
        method: 'POST',
        data: {book_id},
        headers: {'X-CSRFToken': csrftoken},
         beforeSend: function() {
            $('#loader').removeClass('hidden')
        },
        success: function(res){
            if (res.status) {
                window.location.reload();
            }
        }
    })
})
