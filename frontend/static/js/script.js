$('#search_book').on('keyup', function(){
    var search_val = $(this).val();
    var url = $(this).data('url');
    $.ajax({
        method: 'GET',
        url: url,
        data: {search_val},
        beforeSend: function(){
			$("#search_book").css("background", "#FFF url(/media/LoaderIcon.gif) no-repeat 165px");
		},
        success: function(resp) {
            $("#suggesstion-box").show();
			$("#book-list").html(resp.title);
			$("#search_book").css("background", "#FFF");
        },
        error: function(resp){
            console.log(resp)
        }
    })
})

function selectBook(val) {
    $("#search_book").val(val);
    $("#suggesstion-box").hide();
}


function addProduct(book_id){
 var book_id = book_id;
  const csrftoken = getCookie('csrftoken');
  $.ajax({
        url: '/add_to_cart/',
        method: 'POST',
        data: {book_id},
        headers: {'X-CSRFToken': csrftoken},
         beforeSend: function() {
            $('#loader').removeClass('hidden')
        },
        success: function(res){
        console.log(res);
            if (res.status == false) {
                alert('Please login first for go to cart');
            }
           window.location.reload();
        }
    });

//    let products = JSON.parse(window.sessionStorage.getItem("products")) || [];
//    if(products.indexOf(book_id) == -1){
//        products.push(book_id);
//        sessionStorage.setItem('products', JSON.stringify(products));
//    } else {
//        $(document).find('.add_to_cart').hide();
//        $(document).find('.go_to_cart').show();
//    }
//    window.location.reload()
//    console.log(products)
}

function logout(){
    sessionStorage.clear();
}

$(document).ready(function(){
var book_data = new Array();
  const csrftoken = getCookie('csrftoken');
    $.ajax({
        url: '/get_cart_product/',
        method: 'GET',
        headers: {'X-CSRFToken': csrftoken},
        success: function(res){
         console.log(res);
            if (res.status) {

                $('.cartItem').text(res.data.length);
                $('.cartItemId').val(res.data[0].book+',');
                var book_id = $(document).find('.add_to_cart').data('id');
                //    if(products.indexOf(book_id) == -1){
                for(var i=0; i<res.data.length; i++)
                {
                book_data.push(res.data[i].book);
                }
                if( book_data.indexOf(book_id) != -1 ){
                   $(document).find('.add_to_cart').hide();
                   $(document).find('.go_to_cart').show();
                }else{
                    $(document).find('.add_to_cart').show();
                   $(document).find('.go_to_cart').hide();
                }
            }
        }
    });
//
//
//    var book_id = $(document).find('.add_to_cart').data('id');
//    if(products.indexOf(book_id) == -1){
//       $(document).find('.add_to_cart').show();
//       $(document).find('.go_to_cart').hide();
//    }else{
//       $(document).find('.add_to_cart').hide();
//       $(document).find('.go_to_cart').show();
//    }
});

$('.go_to_cart').click(function(){
    $('#added_cart').trigger('submit');
})


$('#added_cart').submit(function(){
    var user_id = $('.user_logged_in_id').val();
    if(user_id == 'None'){
        alert('Please login first for go to cart');
    }
})

$('.cart_icon').click(function(){
   $(this).parent('form').submit();
})

$('.remove_cart').on('click', function(){
    var id = $(this).data('id');
    var id = id;

  const csrftoken = getCookie('csrftoken');
  $.ajax({
        url: '/remove_cart_product/',
        method: 'POST',
        data: {id},
        headers: {'X-CSRFToken': csrftoken},
         beforeSend: function() {
            $('#loader').removeClass('hidden')
        },
        success: function(res){
         console.log(res.messge);
            if (res.status) {
                window.location.reload();
            }
        }
    });
//    let products = JSON.parse(window.sessionStorage.getItem("products"));
//    products = products.filter(item => item !== id)
//    console.log("===============")
//    console.log(products)
//    console.log("===============")
//    sessionStorage.setItem('products', JSON.stringify(products));
//    let new_products = JSON.parse(window.sessionStorage.getItem("products"));
//    $('.cartItemId').val(new_products+',')
//    $('#added_cart').trigger('submit')
})