from django import template
import datetime as dt
from django.conf import settings
register = template.Library()


@register.filter(is_safe=True)
def filter_by_timezone(value, local_tz):
    try:
        if local_tz == settings.TIME_ZONE:
            value = list(filter(lambda x: x.get('territory_out') == False, value))
        else:
            value = list(filter(lambda x: x.get('territory_out') == True, value))
        return value
    except Exception as e:
        print(str(e))
        return value


@register.filter
def sort_by(value, order):
    sorted_val = sorted(value, key=lambda k: k[order], reverse=True)
    return sorted_val



def create_query_objects(queryset):
    new_queryset = []
    for pay in queryset:
        for book in pay.book.all():
            data = {}
            data['book_id'] = book.id
            data['cover_file'] = book.cover_file
            data['cover_file_url'] = book.cover_file.url
            data['title'] = book.title
            data['price_in_inr'] = book.price_in_inr
            data['price_by_currency'] = book.price_by_currency
            new_queryset.append(data)
    return new_queryset

@register.filter
def check_assign_time(queryset, request):
    current_date = dt.datetime.now()
    new_queryset = []
    if request.user.user_type == 2:
        queryset1 = queryset.filter(payment_method='assign', start_date__lt=current_date, end_date__gt=current_date)
        queryset2 = queryset.exclude(payment_method='assign')
        data_list1 = create_query_objects(queryset1)
        data_list = create_query_objects(queryset2)
        data_list1.extend(data_list)
        new_queryset = data_list1
    else:
        new_queryset = create_query_objects(queryset)
    return new_queryset
