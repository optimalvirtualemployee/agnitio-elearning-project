from api.views import ResetPasswordCompleteView
from django.urls import path, include
from .views import *
from .rozar_payment_view import *
app_name = 'frontend'

urlpatterns = [
    path('SearchBook/', SearchBookView.as_view(), name='search_book'),
    path('Library/', LibraryView.as_view(), name='library'),
    path('contact-us/', ContactView.as_view(), name='contact_us'),
    path('about/', AboutView.as_view(), name='about'),
    path('news_listing/', NewsListingView.as_view(), name='news_listing'),
    path('news_detail/<int:pk>/', NewsDetailView.as_view(), name='news_detail'),
    path('', HomeView.as_view(), name='home'),
    path('wish_list/', wishlist, name='wish_list'),
    path('del_wish_list/', del_wishlist, name='del_wish_list'),
    path('add_to_cart/', add_to_cart, name='add_to_cart'),
    path('get_cart_product/', get_cart_product, name='get_cart_product'),
    path('remove_cart_product/', remove_cart_product, name='remove_cart_product'),
    path('ebook_viewer/<int:pk>/', epub_reader, name='epub_reader'),
    path('view_ebook/<int:pk>/', purchased_epub_reader, name='purchased_epub_reader'),
    path('login/', LoginView.as_view(), name='login'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('logout/', logout_view, name='logout'),
    path('forget_password/', ForgetPasswordView.as_view(), name='forget_password'),
    path('reset-password-confirm/<str:uidb64>/<str:token>', ResetPasswordConfirmView.as_view(), name='reset-password-confirm'),
    path('reset_password_complete/', PasswordResetCompleteView.as_view(), name='reset_password_complete'),
    path('activate/<str:uidb64>/<str:token>',activate, name='activate'),
    path('cart/', CartView.as_view(), name='cart'),
    path('wishlist/', WishlistView.as_view(), name='wishlist'),
    path('book-detail/<int:pk>/', BookDetailView.as_view(), name='book_detail'),
    path('book_detail/<int:pk>/', BuyBookDetailView.as_view(), name='login_book_detail'),
    path('book-listing/', BookListingView.as_view(), name='book_listing'),
    path('paymenthandler/', paymenthandler, name='paymenthandler'),
    path('order/', order, name="order"),
    path('successpayment/', successpayment, name="successpayment"),
    # path('order_place', order_place, name='order_place'),
    path('order_place/', order_place, name='homepage'),
    path('terms-condition/', TermsView.as_view(), name='terms-condition'),
    path('return-policy/', ReturnpolicyView.as_view(), name='return-policy'),
    path('test-email-template/', EmailTempate, name='EmailTempate'),
    path('email_confirmation_sent/',email_confirmation_sent_view,name="email_confirmation_sent")
]
