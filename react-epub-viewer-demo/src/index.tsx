import ReactDOM from 'react-dom';
import Reader from 'containers/Reader'

const queryParams = new URLSearchParams(window.location.search);

const id = queryParams.get('id');
const type = queryParams.get('type');
console.log(id, type)
const BASE_URL = 'http://agnitio.optimaldevelopments.com:8000'
// const BASE_URL = 'http://127.0.0.1:8000'

const URL = `${BASE_URL}/api/book-detail/${id}/`
var EPUB_URL = ''
fetch(URL, {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
})
    .then(response => response.json())
    .then(res => {
		if(type == 1){
        EPUB_URL = `${BASE_URL}${res.library.epub_file}`
		} else {
        EPUB_URL = `${BASE_URL}${res.library.epub_sample_file}`
		}
		console.log(EPUB_URL)
        ReactDOM.render(<Reader url={EPUB_URL} />, document.getElementById('root'));

    });
// const EPUB_URL1 = "/react-epub-viewer/files/Alices Adventures in Wonderland.epub";
// const EPUB_URL1 = "/react-epub-viewer/files/moby-dick (1).epub";